/* jshint strict: true */

'use strict';

const
  _ = require('lodash'),
  config = require('./../config'),
  defaultLanguage = 'ru',
  commonLanguage = 'en',
  languageList = {
    'az' : 'Azerbaijan',
    'ru' : 'Russian',
    'en' : 'English'
  },
  languageListTranslatedTo = {
    'en' : languageList,
    'az' : {
      'az' : 'Azərbaycan',
      'ru' : 'Rus',
      'en' : 'İngilis'
    },
    'ru' : {
      'az' : 'Азербайджанский',
      'ru' : 'Русский',
      'en' : 'Английский'
    },
  };

class Constants {
  static defaultLanguage() {
    return defaultLanguage;
  }
  
  static commonLanguage() {
    return commonLanguage;
  }
  
  static languageList() {
    return languageList;
  }
  
  static isAcceptedLang(lang) {
    return languageList.hasOwnProperty(lang);
  }
  
  static languages(lang = defaultLanguage) {
    return languageListTranslatedTo[lang];
  }
}

module.exports = Constants;