const
  _ = require('lodash'),
  async = require('async');

class Shell {
  constructor(stdin, stdout) {
    this._stdin = stdin;
    this._stdout = stdout;
  }

  askOne(question, callback) {
    let self = this;

    self._stdin.resume();
    self._stdout.write((question.required === false ? '(Optional) ': '') + question.info + ": ");

    self._stdin.once('data',
      (data) => {
        let response = data.toString().trim();

        if (question.required !== false && response === '') {
          return self.askOne(question, callback); // ask again
        }

        if(!_.isUndefined(question.default)) {
          response = question.default;
        }

        self._stdin.pause();
        callback(response);
      });
  }

  askMany(questions, callback) {
    let
      self = this,
      answers = {},
      fns = [];

    _.forEach(questions,
      (question, q) => {
        fns.push((next) => {
          self.askOne(question, (answer) => {
            if(!_.isEmpty(answer)) {
              answers[q] = answer;
            }
            next();
          });
        });
      });
    async.series(fns, (err, results) => callback(answers));
  }
}

module.exports = new Shell(process.stdin, process.stdout);