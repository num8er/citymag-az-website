/* jshint strict: true */

'use strict';

const
  _ = require('lodash'),
  md5 = require('./hasher').md5,
  config = require('../config'),
  redis = require('./redis');

class CacheManager {
  constructor(prefix, ttl, driver) {
    this._prefix = prefix;
    this._ttl = ttl;
    this._driver = driver;
    this._cache = {};
  }

  generateKey(key) {
    let resultKey = '';

    // if key is request - generate unique key using route, params and user role
    if (typeof(key) === 'object' && key.route) {
      resultKey = md5(
        JSON.stringify({
          path: key.route.path,
          query: key.query,
          params: key.params,
          body: key.body,
          roles: (key.user) ? key.user.roles : null
        })
      );
    }
    else {
      resultKey = key;
    }

    return this._prefix + resultKey;
  }

  set(key, data, callback) {
    let
      ttl = this._ttl;
      key = this._prefix + ':' + key;

    switch (this._driver) {
      case 'redis':
        redis.set(
          key, JSON.stringify(data),
          (err) => {
            if (!err) {
              redis.expire(key, ttl, () => {});
            }

            if (callback) {
              callback(err);
            }
          });
        return;
    }

    // by default memory
    this._cache[key] = {
      expires: (new Date().getTime()) + ttl * 1000,
      data: data
    };

    if (callback) {
      callback();
    }
  }

  get(key, callback) {
    key = this._prefix + ':' + key;

    switch (this._driver) {
      case 'redis':
        redis.get(
          key,
          (err, data) => {
            if (err) {
              return callback(err, null);
            }

            callback(null, (data) ? JSON.parse(data) : null);
          });
        return;
    }

    // by default memory
    if (this._cache[key]) {
      if (this._cache[key].expires > (new Date().getTime())) {
        return callback(null, this._cache[key].data);
      }

      delete this._cache[key];
    }

    callback(null, null);
  }

  purge (callback) {
    redis.keys(
      this._prefix + ':*',
      (err, keys) => {
        if (keys && keys.length < 1) {
          if (callback) {
            callback(null, 0);
          }
          return;
        }

        redis.del(
          keys,
          (err, count) => {
            if (callback) {
              callback(err, count || 0);
            }
          });
      });
  }
}

module.exports =
  new CacheManager(config.get('cache:prefix'), config.get('cache:ttl'), config.get('cache:driver'));