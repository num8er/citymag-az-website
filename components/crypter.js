/* jshint strict: true */

'use strict';

const
  config = require('./../config'),
  crypto = require('crypto');

class Crypter {
  static getSecretKey() {
    return config.get('secretKey');
  }
  
  static encrypt(data) {
    let cipher = crypto.createCipher('aes-256-ctr', this.getSecretKey());
    let crypted = cipher.update(data, 'utf8', 'hex');
        crypted += cipher.final('hex');
    return crypted;
  }

  static decrypt(data) {
    let decipher = crypto.createDecipher('aes-256-ctr', this.getSecretKey());
    let decrypted = decipher.update(data, 'hex', 'utf8');
        decrypted += decipher.final('utf8');
    return decrypted;
  }
}

module.exports = Crypter;