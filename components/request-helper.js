/* jshint strict: true */

'use strict';

const
  _ = require('lodash'),
  async = require('async'),
  moment = require('moment'),
  cache = require('./cache-manager'),
  db = require('./database'),
  validator = require('validator');

const
  AccessToken = db.model('AccessToken'),
  User = db.model('User');

const validationRules = {
  default: {
    query: {
      dateFrom: "date:format(YYYY-MM-DDTHH:mm:ssZ)|max:now|optional",
      dateTo: "date:format(YYYY-MM-DDTHH:mm:ssZ)|optional"
    }
  },
  pagination: {
    query: {
      size: "number|optional|default:100",
      from: "number|optional|default:0",
      sortBy: "string|optional",
      sort: "string:valid(asc,desc)|optinal|default:asc"
    }
  }
};

function sendResponse(res, data) {
  if (data._cacheInfo) {
    res.setHeader('ETag', data._cacheInfo.eTag);
    res.setHeader('Last-Modified', data._cacheInfo.lastModified);
    delete data._cacheInfo;
  }

  res.send(data);
}

function passThroughValidationAttributes(attributes, rules, res) {
  let field, rule, condition, conditions, conditionOptions, c;

  for (field in rules) {
    rule = rules[field];
    if (!_.isString(rule)) {
      continue;
    }

    conditions = rule.split('|');

    if (_.indexOf(conditions, 'required') > -1) {
      if (_.isUndefined(attributes[field])) {
        return res.status(400).send({
          success: false,
          error: field + ' is required'
        });
      }
    }
    else {
      if (_.isUndefined(attributes[field])) {
        continue;
      }
    }

    for (c = 0; c < conditions.length; c+=1) {
      condition = conditions[c];
      if (!_.isString(condition)) {
        continue;
      }
      conditionOptions = condition.split(':');
  
      if (_.indexOf(conditionOptions, 'string') > -1) {
        if (!_.isString(attributes[field])) {
          return res.status(400).send({
            success: false,
            error: field + ' must be a string'
          });
        }
      }

      if (_.indexOf(conditionOptions, 'email') > -1) {
        if (!validator.isEmail(attributes[field])) {
          return res.status(400).send({
            success: false,
            error: field + ' must be an email'
          });
        }
      }

      if (
        _.indexOf(conditionOptions, 'num') > -1 ||
        _.indexOf(conditionOptions, 'number') > -1 ||
        _.indexOf(conditionOptions, 'numeric') > -1
      ) {
        if (!_.isNaN(attributes[field])) {
          return res.status(400).send({
            success: false,
            error: field + ' must be a number'
          });
        }
      }

      if (
        _.indexOf(conditionOptions, 'int') > -1 ||
        _.indexOf(conditionOptions, 'integer') > -1
      ) {
        if (!_.isInteger(attributes[field])) {
          return res.status(400).send({
            success: false,
            error: field + ' must be a integer'
          });
        }
      }
  
      if (
        _.indexOf(conditionOptions, 'obj') > -1 ||
        _.indexOf(conditionOptions, 'object') > -1 ||
        _.indexOf(conditionOptions, 'mixed') > -1
      ) {
        if (!_.isObject(attributes[field])) {
          return res.status(400).send({
            success: false,
            error: field + ' must be a object'
          });
        }
      }
    }
  }

  return true;
}

function passThroughValidationRules(rules, req, res, callback) {
  for(let element of ['params', 'query', 'body']) {
    if(passThroughValidationAttributes(req[element], rules[element], res) !== true) {
      return;
    }
  }
  
  callback();
}

module.exports.validation = (args) => {
  let rules = _.extend({
    params: {},
    body: {},
    query: {}
  }, _.pick(args, ['params', 'body', 'query']));

  _.each(validationRules, (validationRule) => {
    rules = _.extend(rules, validationRule);
  });

  return (req, res, next) =>
    passThroughValidationRules(rules, req, res, next);
};

module.exports.isAuthenticated = (req, res, next) => {
  this.initializeUser(req, user => {
    if (user) {
      return next();
    }
    
    res.status(403).send({
      success: false,
      error: 'Forbidden'
    });
  });
};

module.exports.initializeUser = (req, callback) => {
  AccessToken.findById(req.accessToken, (err, result) => {
    if(err) {
      return callback(err);
    }
    
    if(!result) {
      return callback();
    }
    
    let
      query = {
        _id: result.user,
        active: true,
        deleted: false
      };
    User.findOne(query, (err, result) => {
      if(!err && result) {
        req.session.user = result;
        req.user = result;
        return callback(result);
      }
      
      callback();
    });
  });
};

module.exports.redirectIfNotAuthenticated = uri => {
  let self = this;
  return (req, res, next) => {
    self.initializeUser(req, user => {
      if(user) {
        return next();
      }
      res.redirect(uri);
    });
  };
};

// TODO: Implement checkRole, for now just Unauthorized
module.exports.checkRole = (req, res) => {
  res.status(401).send('Unauthorized');
};