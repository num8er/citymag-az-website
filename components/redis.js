/* jshint strict: true */

'use strict';

const
  _ = require('lodash'),
  redis = require('redis'),
  config = require('../config/');

let
  client;

if (config.get('redis:autoconnect') === true) {
  client = redis.createClient(config.get('redis:port'), config.get('redis:host'));
  if (!_.isEmpty(config.get('redis:password'))) {
    client.auth(config.get('redis:password'),
      err => (err) ?
        console.error('Redis auth:', err)
        : console.log('Redis authenticated'));
  }

  client.on('connect', () => console.log('Redis connected'));
  client.on('error', error => console.error(error));
}

module.exports = client;