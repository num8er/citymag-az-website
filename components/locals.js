/* jshint strict: true */

'use strict';

const
  _ = require('lodash'),
  constants = require('./constants'),
  db = require('./database'),
  MenuElement = db.model('MenuElement'),
  Category = db.model('Category');

let locals = {};

class LocalsCache {
  static clear() {
    locals = {
      default: {}
    };
  }
  
  static set(key, value, lang = null) {
    if (lang) {
      if(!constants.isAcceptedLang(lang)) {
        lang = constants.defaultLanguage();
      }
    
      if(!locals[lang]) {
        locals[lang] = {};
      }
      
      return locals[lang][key] = value;
    }
  
    return locals['default'][key];
  }
  
  static get(key, lang = null) {
    if (lang) {
      if(!constants.isAcceptedLang(lang)) {
        lang = constants.defaultLanguage();
      }
      
      if(!(locals[lang] && locals[lang][key])) {
        return null;
      }
      
      return locals[lang][key];
    }
    
    return locals['default'][key];
  }
}

LocalsCache.clear();

class Locals {
  
  static clear() {
    LocalsCache.clear();
  }
  
  static getRootMenu(lang, done) {
    let
      cached = LocalsCache.get('rootMenu', lang);
      if(cached) {
        return done(null, cached);
      }
    
    let
      query = {
        nestingLevel: 1,
        active: true,
        deleted: false,
      };
    MenuElement
      .find(query)
      .sort({order: 1})
      .exec((err, menuElements) => {
        if(!_.isEmpty(menuElements)) {
          menuElements = _.map(menuElements, menuElement => {
            menuElement.title =
              (menuElement.title.isMultilingual) ?
                menuElement.title.multilingual[lang] :
                menuElement.title.common;
          
            menuElement.slug =
              (menuElement.slug.isMultilingual) ?
                menuElement.slug.multilingual[lang] :
                menuElement.slug.common;
          
            menuElement.link = '/menu/'+menuElement.slug+'/'+lang;
            menuElement = _.pick(menuElement, ['title', 'slug', 'link']);
            return menuElement;
          });
        }
        
        LocalsCache.set('rootMenu', menuElements, lang);
        done(null, menuElements);
      });
  }
  
  static getCategories(lang, done) {
    let
      cached = LocalsCache.get('categories', lang);
      if(cached) {
        return done(null, cached);
      }
  
    let
      query = {
        nestingLevel: 1,
        active: true,
        deleted: false,
      };
    Category
      .find(query)
      .populate('children')
      .sort({'title.common': 1})
      .exec((err, categories) => {
        if (!_.isEmpty(categories)) {
          categories = _.map(categories, category => {
            category.children =
              _.filter(category.children,
                child => (_.isObject(child) && child.deleted === false));
            return category;
          });
  
          categories = _.map(categories, category => {
            category.title =
              (category.title.isMultilingual) ?
                category.title.multilingual[lang] :
                category.title.common;
  
            category.slug =
              (category.slug.isMultilingual) ?
                category.slug.multilingual[lang] :
                category.slug.common;
  
            category.link = '/'+category.slug+'/'+lang;
            
            category.children = _.map(category.children, child => {
              child.title =
                (child.title.isMultilingual) ?
                  child.title.multilingual[lang] :
                  child.title.common;
  
              child.slug =
                (child.slug.isMultilingual) ?
                  child.slug.multilingual[lang] :
                  child.slug.common;
  
              child.link = '/'+child.slug+'/'+lang;
              return _.pick(child, ['title', 'slug', 'link']);
            });
  
            return _.pick(category, ['title', 'slug', 'link', 'children']);
          });
        }
  
        LocalsCache.set('categories', categories, lang);
        done(null, categories);
      });
  }
}

module.exports = Locals;