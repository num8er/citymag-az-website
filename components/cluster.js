/* jshint strict: true */

'use strict';

module.exports.start = (callable) => {
  const
    config = require('../config'),
    cluster = require('cluster'),
    numCPUs = require('os').cpus().length,
    instances = config.get('app:useCluster') ? numCPUs : 1;
  
  process.on('uncaughtException',
    (err) => {
      console.error('uncaughtException:', err.message);
      console.error(err.stack);
    });
  cluster.on('exit',
    (worker, code, signal) => console.log('CLUSTER EXIT', code, signal));
  
  if (instances === 1 || !cluster.isMaster) {
    return callable();
  }
  
  console.log('Starting', instances, 'instances');
  for (let i = 0; i < instances; i++, cluster.fork());
};