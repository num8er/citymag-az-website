**NGINX host config**

```
server {
    listen 80;
    server_name mag.citylife.az;
    rewrite ^ https://$server_name$request_uri? permanent;
}

server {
    listen 443;

    client_max_body_size 2048M;
    client_body_buffer_size 2048M;
    disable_symlinks off;

    server_name mag.citylife.az;
    root /home/citymag/public;

    ssl on;
    ssl_certificate    /home/citymag/certificates/server.crt;
    ssl_certificate_key    /home/citymag/certificates/server.key;
    ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
    ssl_ciphers "ECDHE-RSA-AES256-GCM-SHA384:ECDHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-SHA384:ECDHE-RSA-AES128-SHA256:ECDHE-RSA-AES256-SHA:ECDHE-RSA-AES128-SHA:DHE-RSA-AES256-SHA256:DHE-RSA-AES128-SHA256:DHE-RSA-AES256-SHA:DHE-RSA-AES128-SHA:ECDHE-RSA-DES-CBC3-SHA:EDH-RSA-DES-CBC3-SHA:AES256-GCM-SHA384:AES128-GCM-SHA256:AES256-SHA256:AES128-SHA256:AES256-SHA:AES128-SHA:DES-CBC3-SHA:HIGH:!aNULL:!eNULL:!EXPORT:!DES:!MD5:!PSK:!RC4";
    ssl_session_cache shared:SSL:1m;

    index index.html index.htm;

    location @app {
        log_not_found off;
        access_log off;
        proxy_pass https://127.0.0.1:8443;
        proxy_http_version 1.1;
        proxy_set_header X-Real-IP  $remote_addr;
        proxy_set_header Host $host;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_cache_bypass $http_upgrade;
    }

    location / {
        try_files $uri $uri/ @app;
    }
}
```