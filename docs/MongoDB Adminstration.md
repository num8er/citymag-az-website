**MongoDB config (/etc/mongodb.conf)**

```
storage:
  dbPath: /var/lib/mongodb
  journal:
    enabled: true
  engine: "wiredTiger"

systemLog:
  destination: file
  logAppend: true
  path: /var/log/mongodb/mongod.log

net:
  port: 27017
  bindIp: 127.0.0.1 # comment this line if You need remote access

security:
  authorization: enabled # comment this line before creating admin user and after uncomment it
```
---

**Creating root user and defining any database administration role**

```
use admin
db.getUsers()
db.dropUser('root')
db.createUser(
    {
        user: "root",
        pwd: "some password for root user",
        roles: [
            { role: "root", db: "admin" },
            { role: "dbAdminAnyDatabase", db: "admin" }
        ]
    }
)
db.getUsers()
```
---

**Creating custom user and defining as owner of custom database**

```
use citymag
db.getUsers()
db.dropUser('citymag')
db.createUser(
    {
        user: "citymag",
        pwd: "some password for citymag",
        roles: [
            { role: "dbOwner", db: "citymag" }
        ]
    }
)
db.getUsers()
```
---