'use strict';

const
  _ = require('lodash'),
  request = require('supertest-as-promised'),
  app = require('./../app');

let
  token,
  user = {
    username: 'anar.k.jafarov@gmail.com',
    password: '123456'
  },
  fakeUser = {
    username: 'fake.user@gmail.com',
    password: 'fakeuserpassword'
  };

describe('App', () => {
  
  describe("Auth", () => {
  
    it("should not pass validation because of request body", done => {
      request(app)
        .post('/auth')
        .send({usr: 'blablabla'})
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(400, err => (err)? done.fail(err) : done());
    });
    
    it("should not authenticate with wrong credentials", done => {
      request(app)
        .post('/auth')
        .send(fakeUser)
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(403, err => (err)? done.fail(err) : done());
    });
    
    it("should authenticate and get token", done => {
      request(app)
        .post('/auth')
        .send(user)
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200, (err, res) => {
          if(err) {
            done.fail(err);
          }
          
          if(!res.body.token) {
            done.fail('No token in response body');
          }
  
          token = res.body.token;
          
          done();
        });
    });
    
    it("should destroy session with Authorization header", done => {
      request(app)
        .delete('/auth')
        .set('Accept', 'application/json')
        .set('Authorization', 'Bearer '+token)
        .expect('Content-Type', /json/)
        .expect(200, err => (err)? done.fail(err) : done());
    });
  
    it("should authenticate and get token", done => {
      request(app)
        .post('/auth')
        .send(user)
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200, (err, res) => {
          if(err) {
            done.fail(err);
          }
        
          if(!res.body.token) {
            done.fail('No token in response body');
          }
        
          token = res.body.token;
        
          done();
        });
    });
  
    it("should destroy session with token in cookies", done => {
      request(app)
        .delete('/auth')
        .set('Accept', 'application/json')
        .set('Cookie', ['token='+token])
        .expect('Content-Type', /json/)
        .expect(200, err => (err)? done.fail(err) : done());
    });
    
  });
});