"use strict";

const
  _ = require('lodash'),
  constants = require('./components/constants'),
  hasher = require('./components/hasher'),
  crypter = require('./components/crypter'),
  db = require('./components/database'),
  shell = require('./components/shell');

let
  UserModel = db.model('User'),
  MenuElementModel = db.model('MenuElement'),
  CategoryModel = db.model('Category'),
  PostModel = db.model('Post'),
  PageModel = db.model('Page');

function menu() {
  shell.askOne({
    info: "\nMenu\n" +
    "[1] Hash data\n" +
    "[2] Encrypt/Decrypt data\n" +
    "[3] User management\n" +
    "[4] Menu elements\n" +
    "[5] Categories\n" +
    "[6] Posts\n" +
    "[7] Pages\n" +
    "[q/x] Quit\n"
  }, input => {
    input = _.trim(input);

    switch (input) {
      case '1' :
        hash();
        break;

      case '2' :
        encdec();
        break;
  
      case '3' :
        users();
        break;
  
      case '4' :
        menuElements();
        break;
  
      case '5' :
        categories();
        break;
      
      case '6' :
        posts();
        break;
  
      case '7' :
        pages();
        break;
      
      case 'x' :
      case 'q' :
        console.log('Bye bye! (:');
        process.exit(0);
        break;

      default:
        menu();
    }
  });
}

function hash() {
  shell.askOne({
    info: "\nHash data\n" +
    "Please enter value to hash"
  }, input => {
    input = _.trim(input);
    console.log('Your data hashed:', hasher.hash(input));

    menu();
  });
}

function encdec() {
  shell.askOne({
    info: "\nEncrypt/Decrypt\n" +
    " [e] Encrypt\n" +
    " [d] Decrypt\n" +
    " [b/m] back to main menu\n" +
    "[q/x] Quit\n"
  }, input => {
    input = _.trim(input);
    switch (input) {
      case 'e' :
        shell.askOne({
          info: "Please enter data to encrypt"
        }, input => {
          input = _.trim(input);
          console.log('Your data encrypted:', crypter.encrypt(input));

          encdec();
        });
        break;

      case 'd' :
        shell.askOne({
          info: "Please enter encrypted data to decrypt"
        }, input => {
          input = _.trim(input);
          console.log('Your data decrypted:', crypter.decrypt(input));

          encdec();
        });
        break;

      case 'b' :
      case 'm' :
        menu();
        break;

      case 'x' :
      case 'q' :
        console.log('Bye bye! (:');
        process.exit(0);
        break;

      default :
        encdec();
    }
  });
}

/*
USERS
 */
function users() {
  shell.askOne({
    info: "\nUser management\n" +
    " Please choose:\n" +
    " [c] create\n" +
    " [r] read (get info)\n" +
    " [u] update (by email)\n" +
    " [d] delete (by email)\n" +
    " [s] search (20 users max)\n" +
    " [l] list (20 users per page)\n" +
    " [b/m] back to main menu\n" +
    "[q/x] Quit\n"
  }, input => {
    input = _.trim(input);
    
    switch (input) {
      case 'c' :
        usersCreate();
        break;
      
      case 'r' :
        usersRead();
        break;
      
      case 'u' :
        usersUpdate();
        break;
      
      case 'd' :
        usersDelete();
        break;
      
      case 's' :
        usersSearch();
        break;
      
      case 'l' :
        usersList();
        break;
      
      case 'b' :
      case 'm' :
        menu();
        break;
      
      case 'x' :
      case 'q' :
        console.log('Bye bye! (:');
        process.exit(0);
        break;
      
      default :
        users();
    }
  });
}

function usersCreate() {
  console.log(
    "User management\n" +
    " Create");
  
  shell.askMany({
    firstname: {
      info: ' Firstname',
      required: true
    },
    lastname: {
      info: ' Lastname',
      required: true
    },
    username: {
      info: ' Email (will be as username)',
      required: true
    },
    password: {
      info: ' Password',
      required: true
    }
  }, input => {
    input.passwordEncrypted = crypter.encrypt(input.password);
    input.password = hasher.hash(input.password);
    UserModel.create(input,
      (err, result) => {
        if (err) {
          console.error('Failed:', err);
          return users();
        }
        
        console.log('Result:', result);
        users();
      });
  });
}

function usersRead() {
  shell.askOne({
    info: "User management\n" +
    " Read (get info)\n" +
    " Please enter email (username)"
  }, input => {
    input = _.trim(input);
  
    UserModel.findOne(input, (err, result) => {
      if (err) {
        console.error('Failed:', err);
        return users();
      }
      
      console.log('Result:', result);
      users();
    });
  });
}

function usersUpdate() {
  shell.askOne({
    info: "User management\n" +
    " Update\n" +
    " Please enter email (username)"
  }, (username) => {
    username = _.trim(username);
    
    UserModel.findOne({username: username}, (err, User) => {
      if (err) {
        console.error('Failed:', err);
        return users();
      }
      
      console.log('Found user:', User);
      console.log('\nLeave fields empty if You want to keep field unchanged');
      
      shell.askMany({
        firstname: {
          info: ' Firstname ' + ((User.firstname != '') ? '[' + User.firstname + ']' : ''),
          required: false
        },
        lastname: {
          info: ' Lastname ' + ((User.lastname != '') ? '[' + User.lastname + ']' : ''),
          required: false
        },
        username: {
          info: ' Email (will be as username) [' + User.username + ']',
          required: false
        },
        password: {
          info: ' Password [hashed: ' + User.password + ']',
          required: false
        }
      }, input => {
        if (_.isEmpty(input)) {
          console.log('Nothing to update (:');
          return users();
        }
        
        input.passwordEncrypted = crypter.encrypt(input.password);
        input.password = hasher.hash(input.password);

        User = _.assign(User, input);
        User.save(
          (err, result) => {
            if (err) {
              console.error('Failed:', err);
              return users();
            }
            
            console.log('Result:', result);
            users();
          });
      });
    });
  });
}

function usersDelete() {
  shell.askOne({
    info: "User management\n" +
    " Delete\n"
  }, input => {
    input = _.trim(input);
    console.log('Sorry, not implemented yet');
    users();
  });
}

function usersSearch() {
  shell.askOne({
    info: "User management\n" +
    " Search\n"
  }, input => {
    input = _.trim(input);
    console.log('Sorry, not implemented yet');
    users();
  });
}

function usersList() {
  shell.askOne({
    info: "User management\n" +
    " List\n"
  }, input => {
    input = _.trim(input);
    console.log('Sorry, not implemented yet');
    users();
  });
}

/*
MENU ELEMENTS
 */
function menuElements() {
  shell.askOne({
    info: "\nMenu elements\n" +
    " Please choose:\n" +
    " [c] create\n" +
    " [r] read (get info)\n" +
    " [u] update (by slug)\n" +
    " [d] delete (by slug)\n" +
    " [s] search (20 entries max)\n" +
    " [l] list (20 entries per page)\n" +
    " [b/m] back to main menu\n" +
    "[q/x] Quit\n"
  }, input => {
    input = _.trim(input);
    
    switch (input) {
      case 'c' :
        menuElementsCreate();
        break;
      
      case 'r' :
        menuElementsRead();
        break;
      
      case 'u' :
        menuElementsUpdate();
        break;
      
      case 'd' :
        menuElementsDelete();
        break;
      
      case 's' :
        menuElementsSearch();
        break;
      
      case 'l' :
        menuElementsList();
        break;
      
      case 'b' :
      case 'm' :
        menu();
        break;
      
      case 'x' :
      case 'q' :
        console.log('Bye bye! (:');
        process.exit(0);
        break;
      
      default :
        menuElements();
    }
  });
}

function menuElementsCreate() {
  shell.askOne({
    info: "Menu elements\n" +
    " Create\n" +
    'Will be menu title multilingual? (y/n)'
  }, input => {
    input = _.trim(input).toLowerCase();
    switch (input) {
      case 'y' :
      case 'ye' :
      case 'yes' :
      case 'yeah' :
      case 'yup' :
        menuElementsCreateMultilingual()
        break;
      
      case 'n' :
      case 'no' :
      case 'nope' :
        menuElementsCreateNonMultilingual();
        break;
  
      default:
        console.log('Please type: y[es] or n[o]')
        menuElementsCreate();
    }
  });
}

function menuElementsCreateMultilingual() {
  let
    languages = constants.languageList(),
    commonLanguage = constants.commonLanguage(),
    questions = {};
    
    for(let lang in languages) {
      questions['title.' + lang] = {
        info: 'Title (language: '+languages[lang]+')',
        required: true
      };
  
      questions['slug.' + lang] = {
        info: 'Slug (language: '+languages[lang]+') (keep empty to generate from title)',
        required: false
      };
    }
    
  shell.askMany(questions, inputs => {
    let
      input = {},
      titles = {},
      slugs = {};
    for(let lang in constants.languageList()) {
      titles[lang] = inputs['title.'+lang];
      slugs[lang] = inputs['slug.'+lang];
      slugs[lang] = _.trim(slugs[lang]);
      if(slugs[lang] == '') {
        slugs[lang] = _.kebabCase(titles[lang]);
      }
    }
    
    input.title = {
      isMultilingual: true,
      common: titles[commonLanguage],
      multilingual: titles
    };
  
    input.slug = {
      isMultilingual: true,
      common: slugs[commonLanguage],
      multilingual: slugs
    };
    
    MenuElementModel
      .findOne({
        nestingLevel: 1,
        deleted: false
      })
      .sort({order: -1})
      .exec((err, result) => {
        input.order = result.order + 1;
        MenuElementModel.create(input,
          (err, result) => {
            if (err) {
              console.error('Failed:', err);
              return menuElements();
            }
      
            console.log('Result:', result);
            console.log('Go to backend interface to select action for menu element');
            menuElements();
          });
      });
  });
}

function menuElementsCreateNonMultilingual() {
  
  shell.askMany({
    title: {
      info: 'Title',
      required: true
    },
    slug: {
      info: 'Slug (keep empty to generate from title)',
      required: false
    }
  }, input => {
    input.slug = _.trim(input.slug);
    if(input.slug == '') {
      input.slug = _.kebabCase(input.title);
    }
  
    let slugs = {};
    for(let lang in constants.languageList()) {
      slugs[lang] = input.slug;
    }
    input.slug = {
      isMultilingual: false,
      common: input.slug,
      multilingual: slugs
    };
  
    let titles = {};
    for(let lang in constants.languageList()) {
      titles[lang] = input.title;
    }
    input.title = {
      isMultilingual: false,
      common: input.title,
      multilingual: titles
    };
  
    MenuElementModel
      .findOne({
        nestingLevel: 1,
        deleted: false
      })
      .sort({order: -1})
      .exec((err, result) => {
        input.order = result.order + 1;
        MenuElementModel.create(input,
          (err, result) => {
            if (err) {
              console.error('Failed:', err);
              return menuElements();
            }
          
            console.log('Result:', result);
            console.log('Go to backend interface to select action for menu element');
            menuElements();
          });
      });
  });
}

function menuElementsRead() {
  shell.askOne({
    info: "Menu elements\n" +
    " Read (get info)\n" +
    " Please enter slug"
  }, input => {
    input = _.trim(input);
    console.log('Sorry, not implemented yet');
    menuElements();
  });
}

function menuElementsUpdate() {
  shell.askOne({
    info: "Menu elements\n" +
    " Update\n" +
    " Please enter slug"
  }, (username) => {
    input = _.trim(input);
    console.log('Sorry, not implemented yet');
    menuElements();
  });
}

function menuElementsDelete() {
  shell.askOne({
    info: "Menu elements\n" +
    " Delete\n" +
    " Please enter slug"
  }, input => {
    input = _.trim(input);
    console.log('Sorry, not implemented yet');
    menuElements();
  });
}

function menuElementsSearch() {
  shell.askOne({
    info: "Menu elements\n" +
    " Search\n" +
    " Please enter search query"
  }, input => {
    input = _.trim(input);
    console.log('Sorry, not implemented yet');
    menuElements();
  });
}

function menuElementsList() {
  shell.askOne({
    info: "Menu elements\n" +
    " Listing\n"
  }, input => {
    input = _.trim(input);
    console.log('Sorry, not implemented yet');
    menuElements();
  });
}

/*
CATEGORIES
 */
function categories() {
  shell.askOne({
    info: "\nCategories\n" +
    " Please choose:\n" +
    " [c] create\n" +
    " [r] read (get info)\n" +
    " [u] update (by slug)\n" +
    " [d] delete (by slug)\n" +
    " [s] search (20 entries max)\n" +
    " [l] list (20 entries per page)\n" +
    " [b/m] back to main menu\n" +
    "[q/x] Quit\n"
  }, input => {
    input = _.trim(input);
    
    switch (input) {
      case 'c' :
        categoriesCreate();
        break;
      
      case 'r' :
        categoriesRead();
        break;
      
      case 'u' :
        categoriesUpdate();
        break;
      
      case 'd' :
        categoriesDelete();
        break;
      
      case 's' :
        categoriesSearch();
        break;
      
      case 'l' :
        categoriesList();
        break;
      
      case 'b' :
      case 'm' :
        menu();
        break;
      
      case 'x' :
      case 'q' :
        console.log('Bye bye! (:');
        process.exit(0);
        break;
      
      default :
        categories();
    }
  });
}

function categoriesCreate() {
  shell.askOne({
    info: "Categories\n" +
    " Create\n" +
    'Will be category title multilingual? (y/n)'
  }, input => {
    input = _.trim(input).toLowerCase();
    switch (input) {
      case 'y' :
      case 'ye' :
      case 'yes' :
      case 'yeah' :
      case 'yup' :
        categoriesCreateMultilingual();
        break;
      
      case 'n' :
      case 'no' :
      case 'nope' :
        categoriesCreateNonMultilingual();
        break;
      
      default:
        console.log('Please type: y[es] or n[o]')
        categoriesCreate();
    }
  });
}

function categoriesCreateMultilingual() {
  let
    languages = constants.languageList(),
    commonLanguage = constants.commonLanguage(),
    questions = {};
  
  for(let lang in languages) {
    questions['title.' + lang] = {
      info: 'Title (language: '+languages[lang]+')',
      required: true
    };
    
    questions['slug.' + lang] = {
      info: 'Slug (language: '+languages[lang]+') (keep empty to generate from title)',
      required: false
    };
  }
  
  shell.askMany(questions, inputs => {
    let
      input = {},
      titles = {},
      slugs = {};
    for(let lang in constants.languageList()) {
      titles[lang] = inputs['title.'+lang];
      slugs[lang] = inputs['slug.'+lang];
      slugs[lang] = _.trim(slugs[lang]);
      if(slugs[lang] == '') {
        slugs[lang] = _.kebabCase(titles[lang]);
      }
    }
    
    input.title = {
      isMultilingual: true,
      common: titles[commonLanguage],
      multilingual: titles
    };
    
    input.slug = {
      isMultilingual: true,
      common: slugs[commonLanguage],
      multilingual: slugs
    };
    
    CategoryModel.create(input,
      (err, result) => {
        if (err) {
          console.error('Failed:', err);
          return categories();
        }
        
        console.log('Result:', result);
        console.log('Go to backend interface to see category and do some ops.');
        categories();
      });
  });
}

function categoriesCreateNonMultilingual() {
  
  shell.askMany({
    title: {
      info: 'Title',
      required: true
    },
    slug: {
      info: 'Slug (keep empty to generate from title)',
      required: false
    }
  }, input => {
    input.slug = _.trim(input.slug);
    if(input.slug == '') {
      input.slug = _.kebabCase(input.title);
    }
    
    let slugs = {};
    for(let lang in constants.languageList()) {
      slugs[lang] = input.slug;
    }
    input.slug = {
      isMultilingual: false,
      common: input.slug,
      multilingual: slugs
    };
    
    let titles = {};
    for(let lang in constants.languageList()) {
      titles[lang] = input.title;
    }
    input.title = {
      isMultilingual: false,
      common: input.title,
      multilingual: titles
    };
    
    CategoryModel
      .findOne({
        nestingLevel: 1,
        deleted: false
      })
      .sort({order: -1})
      .exec((err, result) => {
        input.order = result.order + 1;
        CategoryModel.create(input,
          (err, result) => {
            if (err) {
              console.error('Failed:', err);
              return categories();
            }
            
            console.log('Result:', result);
            console.log('Go to backend interface to see category and do some ops.');
            categories();
          });
      });
  });
}

function categoriesRead() {
  shell.askOne({
    info: "Categories\n" +
    " Read (get info)\n" +
    " Please enter slug"
  }, input => {
    input = _.trim(input);
    console.log('Sorry, not implemented yet');
    categories();
  });
}

function categoriesUpdate() {
  shell.askOne({
    info: "Categories\n" +
    " Update\n" +
    " Please enter slug"
  }, (username) => {
    input = _.trim(input);
    console.log('Sorry, not implemented yet');
    categories();
  });
}

function categoriesDelete() {
  shell.askOne({
    info: "Categories\n" +
    " Delete\n" +
    " Please enter slug"
  }, input => {
    input = _.trim(input);
    console.log('Sorry, not implemented yet');
    categories();
  });
}

function categoriesSearch() {
  shell.askOne({
    info: "Categories\n" +
    " Search\n" +
    " Please enter search query"
  }, input => {
    input = _.trim(input);
    console.log('Sorry, not implemented yet');
    categories();
  });
}

function categoriesList() {
  shell.askOne({
    info: "Categories\n" +
    " Listing\n"
  }, input => {
    input = _.trim(input);
    console.log('Sorry, not implemented yet');
    categories();
  });
}

/*
POSTS
 */
function posts() {
  shell.askOne({
    info: "\nPosts\n" +
    " Please choose:\n" +
    " [c] create\n" +
    " [r] read (get info)\n" +
    " [u] update (by slug)\n" +
    " [d] delete (by slug)\n" +
    " [s] search (20 entries max)\n" +
    " [l] list (20 entries per page)\n" +
    " [b/m] back to main menu\n" +
    "[q/x] Quit\n"
  }, input => {
    input = _.trim(input);
    
    switch (input) {
      case 'c' :
        postsCreate();
        break;
      
      case 'r' :
        postsRead();
        break;
      
      case 'u' :
        postsUpdate();
        break;
      
      case 'd' :
        postsDelete();
        break;
      
      case 's' :
        postsSearch();
        break;
      
      case 'l' :
        postsList();
        break;
      
      case 'b' :
      case 'm' :
        menu();
        break;
      
      case 'x' :
      case 'q' :
        console.log('Bye bye! (:');
        process.exit(0);
        break;
      
      default :
        posts();
    }
  });
}

function postsCreate() {
  shell.askOne({
    info: "Posts\n" +
    " Create\n" +
    'Going to create multilingual post? (y/n)'
  }, input => {
    input = _.trim(input).toLowerCase();
    switch (input) {
      case 'y' :
      case 'ye' :
      case 'yes' :
      case 'yeah' :
      case 'yup' :
        postsCreateMultilingual()
        break;
      
      case 'n' :
      case 'no' :
      case 'nope' :
        postsCreateNonMultilingual();
        break;
      
      default:
        console.log('Please type: y[es] or n[o]')
        postsCreate();
    }
  });
}

function postsCreateMultilingual() {
  let
    languages = constants.languageList(),
    commonLanguage = constants.commonLanguage(),
    questions = {};
  
  for(let lang in languages) {
    questions['title.' + lang] = {
      info: 'Title (language: '+languages[lang]+')',
      required: true
    };
    
    questions['slug.' + lang] = {
      info: 'Slug (language: '+languages[lang]+') (keep empty to generate from title)',
      required: false
    };
  }
  
  shell.askMany(questions, inputs => {
    let
      input = {},
      titles = {},
      slugs = {};
    for(let lang in constants.languageList()) {
      titles[lang] = inputs['title.'+lang];
      slugs[lang] = inputs['slug.'+lang];
      slugs[lang] = _.trim(slugs[lang]);
      if(slugs[lang] == '') {
        slugs[lang] = _.kebabCase(titles[lang]);
      }
    }
    
    input.title = {
      isMultilingual: true,
      common: titles[commonLanguage],
      multilingual: titles
    };
  
    input.body = input.title;
  
    input.slug = {
      isMultilingual: true,
      common: slugs[commonLanguage],
      multilingual: slugs
    };

    PostModel.create(input,
      (err, result) => {
        if (err) {
          console.error('Failed:', err);
          return posts();
        }
        
        console.log('Result:', result);
        console.log('Go to backend interface to manage post');
        posts();
      });
  });
}

function postsCreateNonMultilingual() {
  
  shell.askMany({
    title: {
      info: 'Title',
      required: true
    },
    slug: {
      info: 'Slug (keep empty to generate from title)',
      required: false
    }
  }, input => {
    input.slug = _.trim(input.slug);
    if(input.slug == '') {
      input.slug = _.kebabCase(input.title);
    }
    
    let slugs = {};
    for(let lang in constants.languageList()) {
      slugs[lang] = input.slug;
    }
    input.slug = {
      isMultilingual: false,
      common: input.slug,
      multilingual: slugs
    };
    
    let titles = {};
    for(let lang in constants.languageList()) {
      titles[lang] = input.title;
    }
    input.title = {
      isMultilingual: false,
      common: input.title,
      multilingual: titles
    };
  
    input.body = input.title;
  
    PostModel.create(input,
      (err, result) => {
        if (err) {
          console.error('Failed:', err);
          return posts();
        }
        
        console.log('Result:', result);
        console.log('Go to backend interface to manage post');
        posts();
      });
  });
}

function postsRead() {
  shell.askOne({
    info: "Posts\n" +
    " Read (get info)\n" +
    " Please enter slug"
  }, input => {
    input = _.trim(input);
    console.log('Sorry, not implemented yet');
    posts();
  });
}

function postsUpdate() {
  shell.askOne({
    info: "Posts\n" +
    " Update\n" +
    " Please enter slug"
  }, (username) => {
    input = _.trim(input);
    console.log('Sorry, not implemented yet');
    posts();
  });
}

function postsDelete() {
  shell.askOne({
    info: "Posts\n" +
    " Delete\n" +
    " Please enter slug"
  }, input => {
    input = _.trim(input);
    console.log('Sorry, not implemented yet');
    posts();
  });
}

function postsSearch() {
  shell.askOne({
    info: "Posts\n" +
    " Search\n" +
    " Please enter search query"
  }, input => {
    input = _.trim(input);
    console.log('Sorry, not implemented yet');
    posts();
  });
}

function postsList() {
  shell.askOne({
    info: "Posts\n" +
    " Listing\n"
  }, input => {
    input = _.trim(input);
    console.log('Sorry, not implemented yet');
    posts();
  });
}

/*
PAGES
 */
function pages() {
  shell.askOne({
    info: "\nPages\n" +
    " Please choose:\n" +
    " [c] create\n" +
    " [r] read (get info)\n" +
    " [u] update (by slug)\n" +
    " [d] delete (by slug)\n" +
    " [s] search (20 entries max)\n" +
    " [l] list (20 entries per page)\n" +
    " [b/m] back to main menu\n" +
    "[q/x] Quit\n"
  }, input => {
    input = _.trim(input);
    
    switch (input) {
      case 'c' :
        pagesCreate();
        break;
      
      case 'r' :
        pagesRead();
        break;
      
      case 'u' :
        pagesUpdate();
        break;
      
      case 'd' :
        pagesDelete();
        break;
      
      case 's' :
        pagesSearch();
        break;
      
      case 'l' :
        pagesList();
        break;
      
      case 'b' :
      case 'm' :
        menu();
        break;
      
      case 'x' :
      case 'q' :
        console.log('Bye bye! (:');
        process.exit(0);
        break;
      
      default :
        pages();
    }
  });
}

function pagesCreate() {
  shell.askOne({
    info: "Pages\n" +
    " Create\n" +
    'Going to create multilingual page? (y/n)'
  }, input => {
    input = _.trim(input).toLowerCase();
    switch (input) {
      case 'y' :
      case 'ye' :
      case 'yes' :
      case 'yeah' :
      case 'yup' :
        pagesCreateMultilingual()
        break;
      
      case 'n' :
      case 'no' :
      case 'nope' :
        pagesCreateNonMultilingual();
        break;
      
      default:
        console.log('Please type: y[es] or n[o]')
        pagesCreate();
    }
  });
}

function pagesCreateMultilingual() {
  let
    languages = constants.languageList(),
    commonLanguage = constants.commonLanguage(),
    questions = {};
  
  for(let lang in languages) {
    questions['title.' + lang] = {
      info: 'Title (language: '+languages[lang]+')',
      required: true
    };
    
    questions['slug.' + lang] = {
      info: 'Slug (language: '+languages[lang]+') (keep empty to generate from title)',
      required: false
    };
  }
  
  shell.askMany(questions, inputs => {
    let
      input = {},
      titles = {},
      slugs = {};
    for(let lang in constants.languageList()) {
      titles[lang] = inputs['title.'+lang];
      slugs[lang] = inputs['slug.'+lang];
      slugs[lang] = _.trim(slugs[lang]);
      if(slugs[lang] == '') {
        slugs[lang] = _.kebabCase(titles[lang]);
      }
    }
    
    input.title = {
      isMultilingual: true,
      common: titles[commonLanguage],
      multilingual: titles
    };
    
    input.body = input.title;
    
    input.slug = {
      isMultilingual: true,
      common: slugs[commonLanguage],
      multilingual: slugs
    };
    
    PageModel.create(input,
      (err, result) => {
        if (err) {
          console.error('Failed:', err);
          return pages();
        }
        
        console.log('Result:', result);
        console.log('Go to backend interface to manage page');
        pages();
      });
  });
}

function pagesCreateNonMultilingual() {
  
  shell.askMany({
    title: {
      info: 'Title',
      required: true
    },
    slug: {
      info: 'Slug (keep empty to generate from title)',
      required: false
    }
  }, input => {
    input.slug = _.trim(input.slug);
    if(input.slug == '') {
      input.slug = _.kebabCase(input.title);
    }
    
    let slugs = {};
    for(let lang in constants.languageList()) {
      slugs[lang] = input.slug;
    }
    input.slug = {
      isMultilingual: false,
      common: input.slug,
      multilingual: slugs
    };
    
    let titles = {};
    for(let lang in constants.languageList()) {
      titles[lang] = input.title;
    }
    input.title = {
      isMultilingual: false,
      common: input.title,
      multilingual: titles
    };
  
    input.body = input.title;
  
    PageModel.create(input,
      (err, result) => {
        if (err) {
          console.error('Failed:', err);
          return pages();
        }
        
        console.log('Result:', result);
        console.log('Go to backend interface to manage page');
        pages();
      });
  });
}

function pagesRead() {
  shell.askOne({
    info: "Pages\n" +
    " Read (get info)\n" +
    " Please enter slug"
  }, input => {
    input = _.trim(input);
    console.log('Sorry, not implemented yet');
    pages();
  });
}

function pagesUpdate() {
  shell.askOne({
    info: "Pages\n" +
    " Update\n" +
    " Please enter slug"
  }, (username) => {
    input = _.trim(input);
    console.log('Sorry, not implemented yet');
    pages();
  });
}

function pagesDelete() {
  shell.askOne({
    info: "Pages\n" +
    " Delete\n" +
    " Please enter slug"
  }, input => {
    input = _.trim(input);
    console.log('Sorry, not implemented yet');
    pages();
  });
}

function pagesSearch() {
  shell.askOne({
    info: "Pages\n" +
    " Search\n" +
    " Please enter search query"
  }, input => {
    input = _.trim(input);
    console.log('Sorry, not implemented yet');
    pages();
  });
}

function pagesList() {
  shell.askOne({
    info: "Pages\n" +
    " Listing\n"
  }, input => {
    input = _.trim(input);
    console.log('Sorry, not implemented yet');
    pages();
  });
}

menu();