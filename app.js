/* jshint strict: true */

"use strict";

// LOADING NECESSARY PACKAGES & COMPONENTS
const
  config = require('./config'),
  express = require('express'),
  app = express(),
  cookieParser = require('cookie-parser'),
  bodyParser = require('body-parser'),
  cors = require('cors');

// APPLICATION BOOTSTRAP
app.use('/assets', express.static('public/assets'));
app.set('views', __dirname + '/views');
app.set('view engine', 'pug');
app.set('view cache', config.get('view:cache'));
app.set('trust proxy', 1);
app.use(cors());
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(require('./middlewares/defaultVars'));
app.use(require('./middlewares/catchAccessToken'));
app.use(require('./middlewares/localVars'));
app.use(require('./routes'));

module.exports = app;

if(config.get('app:env') == 'production') {
  process.on('uncaughtException',
    err => console.error(new Date(), 'Got an exception:', err));
}