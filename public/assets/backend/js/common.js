(function($) {
  "use strict";
  
  $.each(["put", "delete"], function(m, method) {
    $[method] = function(url, data, callback, type) {
      if ($.isFunction(data)) {
        type = type || callback;
        callback = data;
        data = undefined;
      }
      
      return $.ajax({
        url: url,
        type: method,
        dataType: type,
        data: data,
        success: callback
      });
    };
  });
  
  $('.masonry').masonry();
})(jQuery);