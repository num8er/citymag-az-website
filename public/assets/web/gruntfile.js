module.exports = function (grunt) {

  require('load-grunt-tasks')(grunt);
  require('time-grunt')(grunt);

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    app: 'src',
    dist: 'dist',

    pug: {
      compile: {
        options: {
          client: false,
          pretty: true
        },
        files: [{
          cwd: "pug/",
          src: "*.pug",
          dest: "./",
          expand: true,
          ext: ".html"
        }]
      }
    },

    postcss: {
      options: {
        map: false,

        processors: [
          require('pixrem')(),
          require('autoprefixer')({browsers: 'last 5 versions'}),
          require('cssnano')()
        ]
      },
      dist: {
        src: 'css/*.css'
      }
    },

    sass: {
      options: {
        sourceMap: true,
        trace: false,
        check: true,
        debugInfo: true,
        noCache: false,
        update: true,
        sourceComments: true,
        quiet: false,
        includePaths: [
          './node_modules/foundation-sites/scss',
          './node_modules/motion-ui/src'
        ]
      },
      dev: {
        options: {
          outputStyle: 'expanded' //nested, compact, compressed, expanded
        },
        files: {
          './css/app.css': 'scss/app.scss',
          './css/foundation.css': 'scss/foundation.scss'
        }
      },
      prod: {
        options: {
          sourceMap: false,
          sourceComments: false,
          outputStyle: 'expanded' //nested, compact, compressed, expanded
        },
        files: {
          '<%= dist %>/css/app.min.css': ['scss/app.scss'],
          '<%= dist %>/css/foundation.min.css': ['scss/foundation.scss']
        }
      }
    },

    php: {
      dist: {
        options: {
          hostname: '127.0.0.1',
          port: 1234,
          base: './',
          keepalive: false,
          open: false
        }
      },
      prod: {
        options: {
          hostname: '127.0.0.1',
          port: 1234,
          base: '<%= dist %>',
          keepalive: true,
          open: true
        }
      }
    },

    browserSync: {
      dist: {
        bsFiles: {
          src: [
            '*.php',
            '*.html',
            'css/app.css',
            'js/*.js',
            'fonts/*'
          ]
        },
        options: {
          proxy: '<%= php.dist.options.hostname %>:<%= php.dist.options.port %>',
          watchTask: true,
          notify: true,
          open: true,
          logLevel: 'silent',
          ghostMode: {
            clicks: true,
            scroll: true,
            links: true,
            forms: true
          }
        }
      }
    },

    watch: {
      sass: {
        files: [
          './scss/base/*.scss',
          './scss/layout/*.scss',
          './scss/module/*.scss',
          './scss/state/*.scss',
          './scss/theme/*.scss',
          './scss/*.scss'
        ],
        tasks: ['sass:dev', 'postcss']
      },
      pug: {
        files: ['./pug/**/*.pug'],
        tasks: 'pug'
      }
    },

    processhtml: {
      dist: {
        options: {
          process: true
        },
        files: {
          '<%= dist %>/index.html': 'index.html'
        }
      }
    },

    uglify: {
      options: {
        mangle: false
      },
      my_target: {
        files: {
          '<%= dist %>/js/app.min.js': [
            './node_modules/jquery/dist/jquery.min.js',
            './js/app.js'
          ]
        }
      }
    },

    copy: {
      main: {
        files: [{
          expand: true,
          flatten: true,
          src: [
            'img/*'
          ],
          dest: '<%= dist %>/img/'
        },

          {
            expand: true,
            flatten: true,
            src: [
              'data/*'
            ],
            dest: '<%= dist %>/data/'
          },
          [{
            expand: true,
            cwd: 'images/',
            src: ['**'],
            dest: '<%= dist %>/images/'
          }]
        ]
      }
    },

    concat: {
      options: {
        separator: '  '
      },
      dist: {
        src: [
          '<%= dist %>/css/foundation.min.css',
          '<%= dist %>/css/app.min.css'
        ],
        dest: '<%= dist %>/css/app.min.css'
      }
    }

  });

  grunt.registerTask('default', [
    'sass:dev',
    'pug',
    'php:dist',
    'browserSync:dist',
    'postcss',
    'watch'
  ]);

  grunt.registerTask('build', [
    'sass:prod',
    'pug',
    'uglify',
    'copy',
    'concat',
    'processhtml'
  ]);

  grunt.registerTask('show', [
    'php:prod'
  ]);

};