$( document ).ready(function() {

  /**
   * Function to init all Ui elements
   */
  function initUiElements() {
    $(document).foundation();

    initMainSlider();
    initContentSliders();
    initScrollableContent();
    initChangeLang();
    initSideAdFlex();
    initSideAdFixed();
  }

  /**
   * Init sidebar advert with fixed position
   */
  function initSideAdFixed() {
    var $sideFixed = $('.js-side-advert-fixed');

    $sideFixed.each(function(index, element) {
      var $el = $(element),
          cachedLeftPosition = $(element).offset().left;

      $el.css({
        left: cachedLeftPosition,
        position: 'fixed',
        top: 0
      });

      // reset changes on window resize
      $( window ).on('resize', function() {

        $el.css({
          left:     $el.hasClass('side-advert-right') ? '101%' : '',
          right:    $el.hasClass('side-advert-left') ? '101%' : '',
          position: 'absolute',
          top: ''
        });

        setTimeout(function() {

          $el.css({
            left: $(element).offset().left,
            position: 'fixed',
            top: 0
          });

        }, 1000);

      });

    });
  }

  /**
   * Init sidebar advert with fluid position
   */
  function initSideAdFlex() {
      var sideFlex = $('.js-side-advert-flex');

      sideFlex.each(function(index, element) {
        var $el = $(element),
            parent = $('.page-wrapper'),
            windowHeight = $(window).height(),
            minHeight = parent.height(),

            scrollableElement = {
              parentElement: parent,
              self: $el,
              width: $el.width(),
              top: 0,
              windowHeight: windowHeight,

              defaultState: {
                width:    $el.width(),
                position: 'absolute',
                left:     $el.hasClass('side-advert-right') ? '101%' : '',
                right:    $el.hasClass('side-advert-left') ? '101%' : '',
                bottom:   '',
                height:   'auto'
              }
            };

        if ( $el.height() < minHeight) {

          // We need to set Timeout to wait pictures to render
          setTimeout(function() {

            // Calculating in start
            calculateScroll(scrollableElement);

            $(window).on('scroll', function() {
              calculateScroll(scrollableElement);
            });

            $( window ).resize(function() {
              $el.css(scrollableElement.defaultState);
            });

          }, 1000);
        }
      });
    }

  /**
   * Init change language drop-down
   */
  function initChangeLang() {
    var changeLangBtn= $('.js-change-lang'),
        langCaret = changeLangBtn.find('.js-header-lang-caret'),
        changeLangDropDown = $('.js-lang-drop-down');

    changeLangBtn.on('click', function() {
      langCaret.toggle();
      changeLangDropDown.toggleClass('hide');
    });

    changeLangDropDown.on('click', function() {
      langCaret.show();
      changeLangDropDown.addClass('hide');
      // Some ajax
    });
  }

  /**
   * Function to init scrollable sidebars
   */
  function initScrollableContent() {
    var $scrollableContents = $('.js-scrollable-content:visible'),
        windowHeight = $(window).height();

      $scrollableContents.each(function(index, elem) {
        var $el = $(elem),
            minHeight = $('.main').height(),

            scrollableElement = {
              parentElement: $('main'),
              self: $el,
              width: $el.width(),
              top: $('.header').height() + 8,
              windowHeight: windowHeight,

              defaultState: {
                position: 'relative',
                left:     '',
                bottom:   '',
                top:      '',
                width:    'auto',
                height:   'auto'
              }
            };

        if ( $el.height() > minHeight) {

          // We need to set Timeout to wait pictures to render
          setTimeout(function() {

            // Calculating in start
            calculateScroll(scrollableElement);

            $(window).on('scroll', function() {
              calculateScroll(scrollableElement);
            });

            $( window ).resize(function() {
              $(elem).css({
                position: 'relative',
                left: '',
                bottom: '',
                top: '',
                width: 'auto',
                height: 'auto'
              });
            });

          }, 1000);
        }
      });
  }

  /**
   * General function to calculate scrolling for content elements
   */
  function calculateScroll(el) {
    var parent = el.parentElement,
        scrollBottom = $(window).scrollTop() + el.windowHeight;

    el.height = el.self.height();
    el.left = el.self.offset().left;
    el.parentBottomTrigger = parent.height() + el.top;
    el.elBottomTrigger = el.top + el.height;

    if( (scrollBottom >= el.elBottomTrigger) && scrollBottom >= el.parentBottomTrigger) {
      el.self.css({
        width: el.width,
        position: 'absolute',
        height: el.height,
        left: '',
        top: el.parentBottomTrigger - el.height
      });

    } else if( (scrollBottom >= el.elBottomTrigger) && scrollBottom <= el.parentBottomTrigger) {
      el.self.css({
        width: el.width,
        position: 'fixed',
        left: el.left,
        bottom: 0,
        top: ''
      });
    } else {
      el.self.css(el.defaultState);
    }
  }

  /**
   * Function to init main slider on index page
   */
  function initMainSlider() {

    // needed to wrap this code in $( window ).load because slider was ugly
    // while rendering
    $( window ).load(function() {
      new Foundation.Orbit($('.js-main-slider'), {
        bullets: true,
        timerDelay: 10000,
        pauseOnHover: false,
        boxOfBullets: 'index--slider--bullets'
      });

      $('.js-index-first-block').removeClass('hide');
    });
  }

  /**
   * general function to all content sliders on index page
   */
  function initContentSliders() {
    var $contentSliders = $('.js-content-slider'),
        options = {
          autoPlay: false,
          bullets: false,
          pauseOnHover: false,
          animInFromLeft:  'fade-in',
          animInFromRight: 'fade-in',
          animOutToLeft:   'fade-out',
          animOutToRight:  'fade-out'
        };

    $contentSliders.each(function(index, element) {
      var localOptions = {
        prevClass: 'js-fade-left-' + index,
        nextClass: 'js-fade-right-' + index
      };

      $(element)
        .parent('.row')
        .find('.js-fade-content-left')
        .addClass(localOptions.prevClass)
        .next()
        .addClass(localOptions.nextClass);

      var currentSlider = new Foundation.Orbit($(element), $.extend({}, options, localOptions));

      $('.' + localOptions.prevClass).on('click', function() {
        currentSlider.changeSlide(false);
      });
      $('.' + localOptions.nextClass).on('click', function() {
        currentSlider.changeSlide(true);
      });

    })
  }


  initUiElements();
});
