/* jshint strict: true */

'use strict';

const
  uuid = require('uuid'),
  mongoose = require('mongoose'),
  Schema = mongoose.Schema;

let
  SlugSchema = {
    isMultilingual: Schema.Types.Boolean,
    common: Schema.Types.String,
    multilingual: Schema.Types.Mixed
  };

let
  TitleSchema = {
    isMultilingual: Schema.Types.Boolean,
    common: Schema.Types.String,
    multilingual: Schema.Types.Mixed
  };
  
module.exports = {
  params: {
    collection: 'categories'
  },
  fields: {
    _id: {
      type: Schema.Types.String,
      index: {
        unique: true
      },
      default: uuid.v4
    },
    slug: {
      type: SlugSchema,
      required: true,
      index: true,
      default: {
        isMultilingual: false,
        common: '',
        multilingual: {}
      }
    },
    title: {
      type: TitleSchema,
      required: true,
      default: {
        isMultilingual: false,
        common: '',
        multilingual: {}
      }
    },
    children: {
      type: [Schema.Types.String],
      index: true,
      default: []
    },
    nestingLevel: {
      type: Schema.Types.Number,
      required: true,
      index: true,
      default: 1
    },
    active: {
      type: Schema.Types.Boolean,
      required: true,
      index: true,
      default: true
    },
    deleted: {
      type: Schema.Types.Boolean,
      required: true,
      index: true,
      default: false
    },
    createdAt: {
      type: Schema.Types.Date,
      default: Date.now
    },
    updatedAt: Schema.Types.Date
  }
};