/* jshint strict: true */

'use strict';

const
  uuid = require('uuid'),
  mongoose = require('mongoose'),
  Schema = mongoose.Schema;

let
  SlugSchema = {
    isMultilingual: Schema.Types.Boolean,
    common: Schema.Types.String,
    multilingual: Schema.Types.Mixed
  };

let
  TitleSchema = {
    isMultilingual: Schema.Types.Boolean,
    common: Schema.Types.String,
    multilingual: Schema.Types.Mixed
  };

module.exports = {
  params: {
    collection: 'menu'
  },
  fields: {
    _id: {
      type: Schema.Types.String,
      index: {
        unique: true
      },
      default: uuid.v4
    },
    slug: {
      type: SlugSchema,
      required: true,
      index: true,
      default: {
        isMultilingual: false,
        common: '',
        multilingual: {}
      }
    },
    title: {
      type: TitleSchema,
      required: true,
      default: {
        isMultilingual: false,
        common: '',
        multilingual: {}
      }
    },
    action: {
      type: Schema.Types.Mixed,
      required: true,
      default: {
        call: 'navigateToRoot',
        args: []
      }
    },
    children: {
      type: [Schema.Types.String],
      index: true,
      default: []
    },
    nestingLevel: {
      type: Schema.Types.Number,
      required: true,
      index: true,
      default: 1
    },
    order: {
      type: Schema.Types.Number,
      required: true,
      index: true,
      default: 1
    },
    active: {
      type: Schema.Types.Boolean,
      required: true,
      index: true,
      default: true
    },
    deleted: {
      type: Schema.Types.Boolean,
      required: true,
      index: true,
      default: false
    },
    createdAt: {
      type: Schema.Types.Date,
      default: Date.now
    },
    updatedAt: Schema.Types.Date
  }
};

/**
 action list:
  1. navigateToRoot()
  2. navigateTo(url)
  3. showPostsByCategoryIds(categoryIds)
 
 nestingLevel:
  1 means it's root element,
  2 means that it's child element of root element,
  "n+1" level is child of "n"-th level element
 
 order:
  means the sequence of showing elements of same nestingLevel
 
 children:
  array of child menu element ids
*/