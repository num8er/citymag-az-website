/* jshint strict: true */

'use strict';

const
  uuid = require('uuid'),
  mongoose = require('mongoose'),
  Schema = mongoose.Schema;

let
  MetadataSchema = {
    loginCount: Schema.Types.Number,
    hasAgreedEula: Schema.Types.Boolean
  },
  ExternalTokensSchema = {
    google: Schema.Types.String,
    facebook: Schema.Types.String,
    twitter: Schema.Types.String,
    vk: Schema.Types.String,
    ok: Schema.Types.String
  };

module.exports = {
  params: {
    collection: 'users'
  },
  fields: {
    _id: {
      type: Schema.Types.String,
      index: {
        unique: true
      },
      default: uuid.v4
    },
    username: {
      type: Schema.Types.String,
      required: true,
      index: true
    },
    password: {
      type: Schema.Types.String,
      required: true
    },
    passwordEncrypted: {
      type: Schema.Types.String,
      required: true
    },
    firstname: {
      type: Schema.Types.String,
      required: true,
      default: ''
    },
    lastname: {
      type: Schema.Types.String,
      required: true,
      default: ''
    },
    roles: {
      type: [Schema.Types.String],
      required: false,
      default: []
    },
    metadata: {
      type: MetadataSchema,
      default: {
        loginCount: 0,
        hasAgreedEula: false
      }
    },
    externalTokens: ExternalTokensSchema,
    active: {
      type: Schema.Types.Boolean,
      required: true,
      index: true,
      default: true
    },
    deleted: {
      type: Schema.Types.Boolean,
      required: true,
      index: true,
      default: false
    },
    createdAt: {
      type: Schema.Types.Date,
      default: Date.now
    },
    updatedAt: Schema.Types.Date
  }
};