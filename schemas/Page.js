/* jshint strict: true */

'use strict';

const
  uuid = require('uuid'),
  mongoose = require('mongoose'),
  Schema = mongoose.Schema;

let
  SlugSchema = {
    isMultilingual: Schema.Types.Boolean,
    common: Schema.Types.String,
    multilingual: Schema.Types.Mixed
  };

let
  TitleSchema = {
    isMultilingual: Schema.Types.Boolean,
    common: Schema.Types.String,
    multilingual: Schema.Types.Mixed
  };

let
  BodySchema = {
    isMultilingual: Schema.Types.Boolean,
    common: Schema.Types.String,
    multilingual: Schema.Types.Mixed
  };
  
module.exports = {
  params: {
    collection: 'pages'
  },
  fields: {
    _id: {
      type: Schema.Types.String,
      index: {
        unique: true
      },
      default: uuid.v4
    },
    slug: {
      type: SlugSchema,
      required: true,
      index: true,
      default: {
        isMultilingual: false,
        common: '',
        multilingual: {}
      }
    },
    title: {
      type: TitleSchema,
      required: true,
      default: {
        isMultilingual: false,
        common: '',
        multilingual: {}
      }
    },
    body: {
      type: BodySchema,
      required: true,
      default: {
        isMultilingual: false,
        common: '',
        multilingual: {}
      }
    },
    mainImage: {
      type: Schema.Types.String,
      ref: 'Image',
      required: false,
      default: null
    },
    images: {
      type: [Schema.Types.String],
      ref: 'Image',
      required: false,
      default: []
    },
    active: {
      type: Schema.Types.Boolean,
      required: false,
      index: true,
      default: false
    },
    deleted: {
      type: Schema.Types.Boolean,
      required: false,
      index: true,
      default: false
    },
    createdAt: {
      type: Schema.Types.Date,
      default: Date.now
    },
    updatedAt: Schema.Types.Date
  }
};