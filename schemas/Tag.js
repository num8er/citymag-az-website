/* jshint strict: true */

'use strict';

const
  uuid = require('uuid'),
  mongoose = require('mongoose'),
  Schema = mongoose.Schema;
 
module.exports = {
  params: {
    collection: 'tags'
  },
  fields: {
    _id: {
      type: Schema.Types.String,
      index: {
        unique: true
      },
      default: uuid.v4
    },
    name: {
      type: Schema.Types.String,
      required: true,
      index: true
    },
    createdAt: {
      type: Schema.Types.Date,
      default: Date.now
    },
    updatedAt: Schema.Types.Date
  }
};