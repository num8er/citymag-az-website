/* jshint strict: true */

'use strict';

module.exports.Page = require('./Page');
module.exports.Post = require('./Post');
module.exports.Category = require('./Category');
module.exports.Tag = require('./Tag');
module.exports.MenuElement = require('./MenuElement');
module.exports.Image = require('./Image');
module.exports.User = require('./User');
module.exports.AccessToken = require('./AccessToken');
