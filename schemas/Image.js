/* jshint strict: true */

'use strict';

const
  uuid = require('uuid'),
  mongoose = require('mongoose'),
  Schema = mongoose.Schema;
 
module.exports = {
  params: {
    collection: 'images'
  },
  fields: {
    _id: {
      type: Schema.Types.String,
      index: {
        unique: true
      },
      default: uuid.v4
    },
    filename: {
      type: Schema.Types.String,
      required: true
    },
    name: {
      type: Schema.Types.String,
      required: true
    },
    extension: {
      type: Schema.Types.String,
      required: false
    },
    contentType: {
      type: Schema.Types.String,
      required: true,
      default: 'application/octet-stream'
    },
    body: {
      type: Schema.Types.Buffer,
      required: true
    },
    deleted: {
      type: Schema.Types.Boolean,
      required: true,
      index: true,
      default: false
    },
    createdAt: {
      type: Schema.Types.Date,
      default: Date.now
    },
    updatedAt: Schema.Types.Date
  }
};