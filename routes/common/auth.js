/* jshint strict: true */

'use strict';

const
  express = require('express'),
  router = express.Router(),
  requestHelper = require('./../../components/request-helper'),
  controllers = require('./../../controllers'),
  Auth = controllers.Common.Auth;

router.post('/',
  requestHelper.validation({
    body: {
      username: "string|required",
      password: "string|required"
    }
  }),
  Auth.attempt);

router.delete('/', Auth.destroy);
router.get('/destroy', Auth.destroy);

module.exports = router;