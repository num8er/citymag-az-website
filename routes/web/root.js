/* jshint strict: true */

'use strict';

const
  express = require('express'),
  router = express.Router(),
  constants = require('./../../components/constants'),
  setLanguage = require('./../../middlewares/setLanguage'),
  initRenderElements = require('./../../middlewares/initRenderElements'),
  controllers = require('./../../controllers'),
  Controller = controllers.Web.Root;

for(let lang in constants.languageList()) {
  router.get('/' + lang, setLanguage(lang), initRenderElements, Controller.handle);
  router.get('/:slug/'+lang, setLanguage(lang), initRenderElements, Controller.proxyTo('Categories'));
  router.get('/:category/:slug/'+lang, setLanguage(lang), initRenderElements, Controller.proxyTo('Posts'));
}
router.get('/', initRenderElements, Controller.handle);
router.get('/:slug', initRenderElements, Controller.proxyTo('Categories'));
router.get('/:category/:slug', initRenderElements, Controller.proxyTo('Posts'));

module.exports = router;