/* jshint strict: true */

'use strict';

const
  express = require('express'),
  router = express.Router(),
  constants = require('./../../components/constants'),
  setLanguage = require('./../../middlewares/setLanguage'),
  controllers = require('./../../controllers'),
  Controller = controllers.Web.Tags;

for(let lang in constants.languageList()) {
  router.get('/' + lang, setLanguage(lang), Controller.handle);
}
router.get('/', Controller.handle);

module.exports = router;