/* jshint strict: true */

'use strict';

const
  express = require('express'),
  router = express.Router(),
  constants = require('./../../components/constants'),
  setLanguage = require('./../../middlewares/setLanguage'),
  controllers = require('./../../controllers'),
  Controller = controllers.Web.Menu;

for(let lang in constants.languageList()) {
  router.get('/:slug/'+lang, setLanguage(lang), Controller.handle);
}
router.get('/:slug', Controller.handle);

module.exports = router;