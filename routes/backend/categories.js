/* jshint strict: true */

'use strict';

const
  express = require('express'),
  router = express.Router();

router.get('/',
  (req, res) => res.render('backend/categories'));

module.exports = router;