/* jshint strict: true */

'use strict';

const
  express = require('express'),
  router = express.Router(),
  controllers = require('./../../controllers'),
  Auth = controllers.Common.Auth;

router.get('/', (req, res) => res.render('backend/auth'));
router.get('/destroy', (req, res) => Auth.destroy(req, res, '/backend/auth', true));

module.exports = router;