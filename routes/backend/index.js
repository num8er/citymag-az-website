/* jshint strict: true */

'use strict';

const
  express = require('express'),
  router = express.Router(),
  requestHelper = require('./../../components/request-helper');

let
  routes = ['auth'],
  guardedRoutes = [
    'dashboard',
    'menu',
    'categories',
    'posts',
    'pages',
    'users',
    'profile'
  ],
  aliases = [
    {route: 'dashboard', alias: ''}
  ];

// defining routes of current sub-route
routes.forEach((item) => router.use('/' + item, require('./' + item)));

// defining routes that will proceed (filtered) through guardFn
let guardFn = requestHelper.redirectIfNotAuthenticated('/backend/auth');
guardedRoutes.forEach(item => router.use('/' + item, guardFn, require('./' + item)));

// defining sub-routes as alias
aliases.forEach(item => {
  if(guardedRoutes.indexOf(item.route) > -1) {
    return router.use('/' + item.alias, guardFn, require('./' + item.route));
  }

  router.use('/' + item.alias, require('./' + item.route));
});

module.exports = router;