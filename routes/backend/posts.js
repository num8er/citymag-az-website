/* jshint strict: true */

'use strict';

const
  express = require('express'),
  router = express.Router(),
  controllers = require('./../../controllers'),
  Controller = controllers.Backend.Posts;

router.get('/',
  (req, res) => res.render('backend/posts/list'));

router.get('/new', Controller.create);

router.get('/:_id',
  (req, res) => res.render('backend/posts/edit'));

module.exports = router;