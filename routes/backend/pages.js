/* jshint strict: true */

'use strict';

const
  express = require('express'),
  router = express.Router();

router.get('/',
  (req, res) => res.render('backend/pages/list'));

router.get('/new',
  (req, res) => res.render('backend/pages/new'));

router.get('/:_id',
  (req, res) => res.render('backend/pages/edit'));

module.exports = router;