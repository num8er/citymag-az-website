/* jshint strict: true */

'use strict';

const
  express = require('express'),
  router = express.Router(),
  requestHelper = require('./../../components/request-helper'),
  controllers = require('./../../controllers'),
  Controller = controllers.Api.Categories;

router.get('/', Controller.readAll);

router.post('/',
  requestHelper.validation({
    body: {
      title: "object|required",
      slug: "object|required",
      isMultilingual: "boolean|optional",
      afterId: "string|optional"
    }
  }),
  Controller.create);

router.get('/:_id',
  requestHelper.validation({
    params: {
      _id: "string|required"
    }
  }),
  Controller.read);

router.put('/:_id',
  requestHelper.validation({
    params: {
      _id: "string|required"
    },
    body: {
      title: "object|optional",
      slug: "object|optional",
      isMultilingual: "boolean|optional",
      afterId: "string|optional"
    }
  }),
  Controller.update);

router.delete('/:_id',
  requestHelper.validation({
    params: {
      _id: "string|required"
    }
  }),
  Controller.delete);

module.exports = router;