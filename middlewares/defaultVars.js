/* jshint strict: true */

'use strict';

const
  _ = require('lodash'),
  constants = require('./../components/constants');

module.exports = (req, res, next) => {
  req.query.q = req.query.q || '';
  req.query.q = _.trim(req.query.q);
  
  req.query.page = parseInt(req.query.page) || 1;
  req.query.page = (req.query.page < 1) ? 1 : req.query.page;
  
  req.lang =
    req.headers['x-lang'] ||
    req.headers['x-language'] ||
    req.query.lang ||
    req.query.language ||
    constants.defaultLanguage();
  if (!constants.isAcceptedLang(req.lang)) {
    req.lang = constants.defaultLanguage();
  }
  
  next();
};