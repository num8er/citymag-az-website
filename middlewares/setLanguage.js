/* jshint strict: true */

'use strict';

const
  constants = require('./../components/constants');

module.exports =
  lang =>
    (req, res, next) => {
      if (!constants.isAcceptedLang(lang)) {
        lang = constants.defaultLanguage();
      }
      req.lang = lang;
      res.locals.lang = req.lang;
      res.locals.languages = constants.languages(req.lang);
      next();
    };