/* jshint strict: true */

'use strict';

module.exports = (req, res, next) => {
  req.lang = req.params.lang || req.params.language;
  res.locals.lang = req.lang;
  
  next();
};