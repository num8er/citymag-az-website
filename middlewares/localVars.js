/* jshint strict: true */

'use strict';

const
  _ = require('lodash'),
  constants = require('./../components/constants'),
  appInfo = require('./../package.json');

module.exports = (req, res, next) => {
  res.locals.req = {};
  for(let element of ['query', 'body']) {
    _.extend(res.locals['req'][element], req[element]);
  }
  
  res.locals.lang = req.lang;
  res.locals.languages = constants.languages(req.lang);
  res.locals.languageList = constants.languageList();
  res.locals.commonLanguage = constants.commonLanguage();
  res.locals.defaultLanguage = constants.defaultLanguage();
  
  res.locals.appInfo = appInfo;

  next();
};