/* jshint strict: true */

'use strict';

const
  async = require('async'),
  constants = require('./../components/constants'),
  locals = require('./../components/locals');

module.exports =
  (req, res, next) =>
    async.parallel([
        done => locals.getRootMenu(req.lang, done),
        done => locals.getCategories(req.lang, done)
      ],
      (err, results) => {
        res.locals.menu = results[0];
        res.locals.categories = results[1];
        res.locals.lang = req.lang;
        res.locals.languages = constants.languages(req.lang);
        next();
      });