/* jshint strict: true */

'use strict';

const
  controllers = require('./../'),
  Controller = controllers.Api.Posts;

class Posts {
  
  static create(req, res) {
    req.body = {
      title: {
        isMultilingual: false,
        common: '',
        multilingual: {}
      },
      body: {
        isMultilingual: false,
        common: '',
        multilingual: {}
      },
      slug: {
        isMultilingual: false,
        common: '',
        multilingual: {}
      },
      author: req.user._id
    };
    Controller.create(req, {
      send(data) {
        res.redirect('/backend/posts/'+data.post._id);
      }
    });
  }
  
  static edit(req, res) {
    
  }
}

module.exports = Posts;