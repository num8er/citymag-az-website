/* jshint strict: true */

'use strict';

const
  _ = require('lodash'),
  async = require('async'),
  hasher = require('./../../components/hasher'),
  constants = require('./../../components/constants'),
  db = require('./../../components/database'),
  MenuElement = db.model('MenuElement'),
  Category = db.model('Category'),
  Page = db.model('Page');

class Categories {
  static handle(req, res) {
    const self = Categories;
  
    res.render('web/category');
  }
}

module.exports = Categories;