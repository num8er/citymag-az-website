/* jshint strict: true */

'use strict';

const
  _ = require('lodash'),
  async = require('async'),
  hasher = require('./../../components/hasher'),
  constants = require('./../../components/constants'),
  db = require('./../../components/database'),
  MenuElement = db.model('MenuElement'),
  Category = db.model('Category'),
  Page = db.model('Page');

class Menu {
  static handle(req, res) {
    const self = Menu;
    async.parallel([
        done => self.searchInMultilingual(req.params.slug, done),
        done => self.searchInCommon(req.params.slug, done)
      ],
      (err, results) => {
        let menuElement = results[0] || results[1];
        if(err || _.isEmpty(menuElement)) {
          return res.redirect('back');
        }

        self.callAction(menuElement.action, req, res);
      });
  }
  
  static searchInMultilingual(slug, done) {
    let query = {
      'slug.isMultilingual': true,
      '$or': []
    };
  
    for(let lang in constants.languageList()) {
      query['$or'].push({['slug.multilingual.'+lang]: slug});
    }
  
    query.active = true;
    query.deleted = false;
  
    MenuElement
      .findOne(query)
      .exec(done);
  }
  
  static searchInCommon(slug, done) {
    MenuElement
      .findOne({
        'slug.isMultilingual' : false,
        'slug.common' : slug,
        active: true,
        deleted: false
      }).exec(done)
  }
  
  static callAction(action, req, res) {
    switch(action['call']) {
      case 'navigateToRoot' : return this.navigateToRoot(req, res);
      case 'navigateToUrl' : return this.navigateToUrl(action['args'], req, res);
      case 'navigateToCategory' : return this.navigateToCategory(action['args'], req, res);
      case 'navigateToPage' : return this.navigateToPage(action['args'], req, res);
    }
  
    res.redirect('back');
  }
  
  static navigateToRoot(req, res) {
    res.redirect(302, '/'+req.lang);
  }
  
  static navigateToUrl(args, req, res) {
    res.redirect(302, _.first(args));
  }
  
  static navigateToCategory(args, req, res) {
    Category
      .findOne({
        _id: _.first(args),
        active: true,
        deleted: false
      })
      .exec((err, category) => {
        if(err || _.isEmpty(category)) {
          return res.redirect('back');
        }
        
        let
          slug =
            (category.slug.isMultilingual)?
              category.slug.multilingual[req.lang]
              : category.slug.common,
          url = '/'+[slug, req.lang].join('/');
        
        res.redirect(302, url);
      });
  }
  
  static navigateToPage(args, req, res) {
    Page
      .findOne({
        _id: _.first(args),
        active: true,
        deleted: false
      })
      .exec((err, page) => {
        if(err || _.isEmpty(page)) {
          return res.redirect('back');
        }
  
        let
          slug =
            (page.slug.isMultilingual)?
              page.slug.multilingual[req.lang]
              : page.slug.common,
          url = '/'+['page', slug, req.lang].join('/');
  
        res.redirect(302, url);
      });
  }
}

module.exports = Menu;