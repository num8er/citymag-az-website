/* jshint strict: true */

'use strict';

const
  _ = require('lodash'),
  async = require('async'),
  hasher = require('./../../components/hasher'),
  constants = require('./../../components/constants'),
  db = require('./../../components/database'),
  MenuElement = db.model('MenuElement'),
  Category = db.model('Category'),
  Page = db.model('Page');

class Pages {
  static handle(req, res) {
    const self = Pages;
  
    res.render('web/page');
  }
}

module.exports = Pages;