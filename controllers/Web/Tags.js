/* jshint strict: true */

'use strict';

const
  _ = require('lodash'),
  async = require('async'),
  hasher = require('./../../components/hasher'),
  constants = require('./../../components/constants'),
  db = require('./../../components/database'),
  MenuElement = db.model('MenuElement'),
  Category = db.model('Category'),
  Page = db.model('Page');

class Tags {
  static handle(req, res) {
    const self = Tags;
  
    res.render('web/tags');
  }
}

module.exports = Tags;