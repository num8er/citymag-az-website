/* jshint strict: true */

'use strict';

const
  _ = require('lodash'),
  async = require('async'),
  constants = require('./../../components/constants'),
  db = require('./../../components/database'),
  Post = db.model('Post');

class Posts {
  static handle(req, res) {
    const self = Posts;
    
    async.parallel([
        done => self.searchInMultilingual(req.params.slug, done),
        done => self.searchInCommon(req.params.slug, done)
      ],
      (err, results) => {
        let post = results[0] || results[1];
        if (err || _.isEmpty(post)) {
          return res.status(404).send('Not found');
        }
        
        if (post.title.isMultilingual) {
          post.title = (post.title.isMultilingual) ?
            post.title.multilingual[req.lang]
            : post.title.common;
        }
        
        if (post.body.isMultilingual) {
          post.body = (post.body.isMultilingual) ?
            post.body.multilingual[req.lang]
            : post.body.common;
          
          post.heading = (post.heading.isMultilingual) ?
            post.heading.multilingual[req.lang]
            : post.heading.common;
        }
        
        post = _.pick(post, [
          'title', 'heading', 'body', 'slug',
          'tags', 'mainImage', 'images'
        ]);
        res.render('web/post', {post});
      });
  }
  
  static searchInMultilingual(slug, done) {
    let query = {
      'slug.isMultilingual': true,
      '$or': []
    };
    
    for (let lang in constants.languageList()) {
      query['$or'].push({['slug.multilingual.' + lang]: slug});
    }
    
    query.active = true;
    query.published = true;
    query.draft = false;
    query.deleted = false;
    
    Post
      .findOne(query)
      .exec(done);
  }
  
  static searchInCommon(slug, done) {
    Post
      .findOne({
        'slug.isMultilingual': false,
        'slug.common': slug,
        active: true,
        published: true,
        draft: false,
        deleted: false
      }).exec(done)
  }
}

module.exports = Posts;