/* jshint strict: true */

'use strict';

module.exports.Menu = require('./Menu');
module.exports.Root = require('./Root');
module.exports.Categories = require('./Categories');
module.exports.Posts = require('./Posts');
module.exports.Pages = require('./Pages');
module.exports.Tags = require('./Tags');