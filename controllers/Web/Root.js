/* jshint strict: true */

'use strict';

const
  _ = require('lodash'),
  async = require('async'),
  hasher = require('./../../components/hasher'),
  constants = require('./../../components/constants'),
  db = require('./../../components/database'),
  MenuElement = db.model('MenuElement'),
  Category = db.model('Category'),
  Page = db.model('Page'),
  Categories = require('./Categories'),
  Posts = require('./Posts');

class Root {
  static handle(req, res) {
    res.render('web/index');
  }
  
  static proxyTo(Controller) {
    return (req, res) => {
      switch (Controller) {
        case 'Categories' : return Categories.handle(req, res);
        case 'Posts' : return Posts.handle(req, res);
      }
      
      res.status(500).send('Proxy controller:', Controller, 'not found');
    }
  }
}

module.exports = Root;