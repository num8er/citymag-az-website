/* jshint strict: true */

'use strict';

const
  _ = require('lodash'),
  hasher = require('./../../components/hasher'),
  db = require('./../../components/database'),
  User = db.model('User'),
  AccessToken = db.model('AccessToken');

class Auth {
  
  static attempt(req, res) {
    let
      query = _.pick(req.body, ['username']);
      query.active = true;
      query.deleted = false;
    User
      .findOne(query, (error, user) => {
        if (error) {
          return res.status(500).send({
            success: false,
            error: error
          });
        }
      
        if(_.isEmpty(user) || !hasher.check(req.body.password, user.password)) {
          return res.status(403).send({
            success: false,
            error: 'Forbidden',
            message: 'Username and/or password is invalid'
          });
        }
  
        AccessToken.create(
          {user: user._id},
          (error, result) => {
            if(error) {
              return res.status(500).send({
                success: false,
                error: error
              });
            }
  
            res.cookie(
              'token',
              result._id,
              {
                maxAge: (req.body.remember)? 31536000000 : 86400000,
                path: '/'
              });
            res.send({
              success: true,
              token: result._id,
              user: user._id,
              metadata: user.metadata
            });
          });
      });
  }
  
  static destroy(req, res, redirectTo='', redirect=false) {
    AccessToken
      .findByIdAndRemove(
        req.accessToken,
        () => {
          if(redirect === true) {
            return res
              .cookie('token', -1, {maxAge: 0, path: '/'})
              .redirect(redirectTo);
          }
          
          res
            .cookie('token', -1, {maxAge: 0, path: '/'})
            .send({success: true});
        });
  }
}

module.exports = Auth;