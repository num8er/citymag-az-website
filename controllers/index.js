/* jshint strict: true */

'use strict';

module.exports.Api = require('./Api');
module.exports.Backend = require('./Backend');
module.exports.Common = require('./Common');
module.exports.Web = require('./Web');