/* jshint strict: true */

'use strict';

const
  _ = require('lodash'),
  async = require('async'),
  hasher = require('./../../components/hasher'),
  db = require('./../../components/database'),
  Page = db.model('Page');

class Pages {
  
  static create(req, res) {
    
  }
  
  static read(req, res) {
    let
      _id = req.params._id;
    
    Page
      .findById(_id)
      .exec((err, page) => {
        res.send({
          success: !_.isEmpty(page),
          page
        });
      });
  }
  
  static readAll(req, res) {
    let
      query = {
        deleted: false
      };
    Page
      .find(query)
      .exec((err, pages) => {
        res.send({
          success: true,
          pages
        });
      });
  }
  
  static update(req, res) {
    
  }
  
  static delete(req, res) {
    let
      _id = req.params._id;
    
    Page
      .findById(_id)
      .exec((err, page) => {
        if(_.isEmpty(page)) {
          return res.send({success: true});
        }
  
        page.deleted = true;
        page.save(() => res.send({success: true}));
      });
  }
}

module.exports = Pages;