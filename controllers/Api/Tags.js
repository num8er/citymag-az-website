/* jshint strict: true */

'use strict';

const
  _ = require('lodash'),
  hasher = require('./../../components/hasher'),
  db = require('./../../components/database'),
  Tag = db.model('Tag');

class Tags {
  
  static create(req, res) {
    
  }
  
  static read(req, res) {
    
  }
  
  static readAll(req, res) {
    
  }
  
  static update(req, res) {
    
  }
  
  static delete(req, res) {
    
  }
}

module.exports = Tags;