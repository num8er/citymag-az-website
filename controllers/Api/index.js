/* jshint strict: true */

'use strict';

module.exports.Categories = require('./Categories');
module.exports.MenuElements = require('./MenuElements');
module.exports.Pages = require('./Pages');
module.exports.Posts = require('./Posts');
module.exports.Tags = require('./Tags');
module.exports.Users = require('./Users');