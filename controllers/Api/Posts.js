/* jshint strict: true */

'use strict';

const
  _ = require('lodash'),
  async = require('async'),
  hasher = require('./../../components/hasher'),
  db = require('./../../components/database'),
  Post = db.model('Post');

class Posts {
  
  static create(req, res) {
    Post.create(req.body,
      (err, post) => {
        res.send({
          success: !_.isEmpty(post),
          post
        });
      });
  }
  
  static read(req, res) {
    let
      _id = req.params._id;
    
    Post
      .findById(_id)
      .exec((err, post) => {
        res.send({
          success: !_.isEmpty(post),
          post
        });
      });
  }
  
  static readAll(req, res) {
    let
      query = {
        deleted: false
      };
    Post
      .find(query)
      .exec((err, posts) => {
        res.send({
          success: true,
          posts
        });
      });
  }
  
  static update(req, res) {
    
  }
  
  static delete(req, res) {
    let
      _id = req.params._id;
    
    Post
      .findById(_id)
      .exec((err, post) => {
        if(_.isEmpty(post)) {
          return res.send({success: true});
        }
        
        post.deleted = true;
        post.save(() => res.send({success: true}));
      });
  }
}

module.exports = Posts;