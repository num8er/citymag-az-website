/* jshint strict: true */

'use strict';

const
  _ = require('lodash'),
  async = require('async'),
  constants = require('./../../components/constants'),
  locals = require('./../../components/locals'),
  db = require('./../../components/database'),
  MenuElement = db.model('MenuElement');

class MenuElements {
  
  static create(req, res) {
    let
      data = _.pick(req.body, ['action', 'slug', 'title']);
  
    MenuElement
      .findOne({
        deleted: false,
        nestingLevel: 1
      })
      .sort({order: -1})
      .exec((err, menuElement) => {
        data.order = menuElement.order || 0;
        data.order++;
  
        switch(data.action['call']) {
          case 'navigateToUrl' :
            data.action['args'] = [data.action['args'].url];
            break;
    
          case 'navigateToCategory' :
            data.action['args'] = [data.action['args'].categoryId];
            break;
    
          case 'navigateToPage' :
            data.action['args'] = [data.action['args'].pageId];
            break;
    
          default:
            data.action['args'] = [];
            break;
        }
  
        let
          titles = {},
          slugs = {};
        if(data.title.isMultilingual) {
          for(let lang in constants.languageList()) {
            titles[lang] = data.title.multilingual[lang];
            slugs[lang] = data.slug.multilingual[lang];
            if(slugs[lang] == '') {
              slugs[lang] = _.kebabCase(titles[lang]);
            }
          }
    
          data.title = {
            isMultilingual: true,
            common: titles[constants.commonLanguage()],
            multilingual: titles
          };
    
          data.slug = {
            isMultilingual: true,
            common: slugs[constants.commonLanguage()],
            multilingual: slugs
          };
        }
        else {
          for(let lang in constants.languageList()) {
            titles[lang] = data.title.common;
            slugs[lang] = data.slug.common;
          }
    
          data.title = {
            isMultilingual: false,
            common: data.title.common,
            multilingual: titles
          };
    
          data.slug = {
            isMultilingual: false,
            common: data.slug.common,
            multilingual: slugs
          };
        }
  
        menuElement = new MenuElement();
        menuElement = _.assign(menuElement, data);
        menuElement.save(() => res.send({success: true, menuElement}));
        locals.clear();
      });
  }
  
  static read(req, res) {
    let
      _id = req.params._id;
  
    MenuElement
      .findById(_id)
      .exec((err, menuElement) => {
        res.send({
          success: !_.isEmpty(menuElement),
          menuElement
        });
      });
  }
  
  static readAll(req, res) {
    let
      query = {
        deleted: false,
        nestingLevel: 1
      };
    MenuElement
      .find(query)
      .populate('children')
      .sort({order: 1})
      .exec((err, menuElements) => {
        if(!_.isEmpty(menuElements)) {
          menuElements = _.map(menuElements, menuElement => {
            menuElement.children =
              _.filter(menuElement.children,
                child => (_.isObject(child) && child.deleted === false));
            return menuElement;
          });
        }
  
        res.send({
          success: true,
          menuElements
        });
      });
  }
  
  static update(req, res) {
    let
      _id = req.params._id;
  
    MenuElement
      .findById(_id)
      .exec((err, menuElement) => {
        if(_.isEmpty(menuElement)) {
          return res.send({
            success: false
          })
        }
        
        let
          data = _.pick(req.body, ['action', 'slug', 'title']);
        
        switch(data.action['call']) {
          case 'navigateToUrl' :
            data.action['args'] = [data.action['args'].url];
            break;
  
          case 'navigateToCategory' :
            data.action['args'] = [data.action['args'].categoryId];
            break;
  
          case 'navigateToPage' :
            data.action['args'] = [data.action['args'].pageId];
            break;
          
          default:
            data.action['args'] = [];
            break;
        }
  
        let
          titles = {},
          slugs = {};
        if(data.title.isMultilingual) {
          for(let lang in constants.languageList()) {
            titles[lang] = data.title.multilingual[lang];
            slugs[lang] = data.slug.multilingual[lang];
            if(slugs[lang] == '') {
              slugs[lang] = _.kebabCase(titles[lang]);
            }
          }
  
          data.title = {
            isMultilingual: true,
            common: titles[constants.commonLanguage()],
            multilingual: titles
          };
  
          data.slug = {
            isMultilingual: true,
            common: slugs[constants.commonLanguage()],
            multilingual: slugs
          };
        }
        else {
          for(let lang in constants.languageList()) {
            titles[lang] = menuElement.title.multilingual[lang] || data.title.common;
            slugs[lang] = menuElement.slug.multilingual[lang] || data.slug.common;
          }
  
          data.title = {
            isMultilingual: false,
            common: data.title.common,
            multilingual: titles
          };
  
          data.slug = {
            isMultilingual: false,
            common: data.slug.common,
            multilingual: slugs
          };
        }
        
        menuElement = _.assign(menuElement, data);
        menuElement.save(() => res.send({success: true, menuElement}));
        locals.clear();
      })
  }
  
  static delete(req, res) {
    let
      _id = req.params._id;
  
    MenuElement
      .findById(_id)
      .exec((err, menuElement) => {
        if(_.isEmpty(menuElement)) {
          return res.send({success: true});
        }
  
        menuElement.deleted = true;
        menuElement.save(() => res.send({success: true}));
        locals.clear();
      });
  }
}

module.exports = MenuElements;