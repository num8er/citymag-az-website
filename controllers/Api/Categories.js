/* jshint strict: true */

'use strict';

const
  _ = require('lodash'),
  async = require('async'),
  constants = require('./../../components/constants'),
  locals = require('./../../components/locals'),
  db = require('./../../components/database'),
  Category = db.model('Category');

class Categories {
  
  static create(req, res) {
    let
      data = _.pick(req.body, ['slug', 'title']);
    
    let
      titles = {},
      slugs = {};
    if(data.title.isMultilingual) {
      for(let lang in constants.languageList()) {
        titles[lang] = data.title.multilingual[lang];
        slugs[lang] = data.slug.multilingual[lang];
        if(slugs[lang] == '') {
          slugs[lang] = _.kebabCase(titles[lang]);
        }
      }
      
      data.title = {
        isMultilingual: true,
        common: titles[constants.commonLanguage()],
        multilingual: titles
      };
      
      data.slug = {
        isMultilingual: true,
        common: slugs[constants.commonLanguage()],
        multilingual: slugs
      };
    }
    else {
      for(let lang in constants.languageList()) {
        titles[lang] = data.title.common;
        slugs[lang] = data.slug.common;
      }
      
      data.title = {
        isMultilingual: false,
        common: data.title.common,
        multilingual: titles
      };
      
      data.slug = {
        isMultilingual: false,
        common: data.slug.common,
        multilingual: slugs
      };
    }
    
    let category = new Category();
    category = _.assign(category, data);
    category.save(() => res.send({success: true, category}));
    locals.clear();
  }
  
  static read(req, res) {
    let
      _id = req.params._id;
    
    Category
      .findById(_id)
      .exec((err, category) => {
        res.send({
          success: !_.isEmpty(category),
          category
        });
      });
  }
  
  static readAll(req, res) {
    let
      query = {
        deleted: false,
        nestingLevel: 1
      };
    Category
      .find(query)
      .populate('children')
      .sort({'title.common': 1})
      .exec((err, categories) => {
        if(!_.isEmpty(categories)) {
          categories = _.map(categories, category => {
            category.children =
              _.filter(category.children,
                child => (_.isObject(child) && child.deleted === false));
            return category;
          });
        }
        
        res.send({
          success: true,
          categories
        });
      });
  }
  
  static update(req, res) {
    let
      _id = req.params._id;
    
    Category
      .findById(_id)
      .exec((err, category) => {
        if(_.isEmpty(category)) {
          return res.send({
            success: false
          })
        }
        
        let
          data = _.pick(req.body, ['action', 'slug', 'title']);
        
        let
          titles = {},
          slugs = {};
        if(data.title.isMultilingual) {
          for(let lang in constants.languageList()) {
            titles[lang] = data.title.multilingual[lang];
            slugs[lang] = data.slug.multilingual[lang];
            if(slugs[lang] == '') {
              slugs[lang] = _.kebabCase(titles[lang]);
            }
          }
          
          data.title = {
            isMultilingual: true,
            common: titles[constants.commonLanguage()],
            multilingual: titles
          };
          
          data.slug = {
            isMultilingual: true,
            common: slugs[constants.commonLanguage()],
            multilingual: slugs
          };
        }
        else {
          for(let lang in constants.languageList()) {
            titles[lang] = category.title.multilingual[lang] || data.title.common;
            slugs[lang] = category.slug.multilingual[lang] || data.slug.common;
          }
          
          data.title = {
            isMultilingual: false,
            common: data.title.common,
            multilingual: titles
          };
          
          data.slug = {
            isMultilingual: false,
            common: data.slug.common,
            multilingual: slugs
          };
        }
  
        category = _.assign(category, data);
        category.save(() => res.send({success: true, category}));
        locals.clear();
      })
  }
  
  static delete(req, res) {
    let
      _id = req.params._id;
    
    Category
      .findById(_id)
      .exec((err, category) => {
        if(_.isEmpty(category)) {
          return res.send({success: true});
        }
        
        category.deleted = true;
        category.save(() => res.send({success: true}));
        locals.clear();
      });
  }
}

module.exports = Categories;