#!/bin/bash

forever stopall
forever start --uid=citymag-az-website-deploy --append deploy.js
forever start --uid=citymag-az-website --append server.js; forever list