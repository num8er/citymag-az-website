/* jshint strict: true */

'use strict';

// LOADING NECESSARY PACKAGES
const
  express = require('express'),
  app = express(),
  exec = require('child_process').exec,
  config = require('./config'),
  Jasmine = require('jasmine'),
  JasmineReporter = require('jasmine-console-reporter'),
  jasmineConfig = require('./spec/support/jasmine.json'),
  jasmineReporterConfig = require('./spec/support/jasmine-console-reporter.json');
  
// INITIALIZING TESTING FRAMEWORK
const
  jasmine = new Jasmine(),
  jasmineReporter = new JasmineReporter(jasmineReporterConfig);

jasmine.loadConfig(jasmineConfig);
jasmine.addReporter(jasmineReporter);
jasmine.onComplete(passed => {
  if(passed) {
    console.log("Tests finished without failures, restarting app with new code\n");
    return exec('./restart.sh');
  }

  console.log("Tests were failed, rolling back to previous commit\n");
  exec('./rollback.sh', () => exec('./restart.sh'));
});


// INITIALIZING DEPLOYMENT LISTENER
let port = config.get('deployment:port');

app.all('/', (req, res) => res.status(200).send('deployment listener'));

app.all('/git/deploy', (req, res) => {
  
  console.log('got deployment request');
  res.status(200).send('ok');
  exec('./deploy.sh', () => jasmine.execute());
});


// BINDING DEPLOYMENT LISTENER TO PORT
app.listen(port, () => console.log('deploy server started at port:', port));