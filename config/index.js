const
  nconf = require('nconf'),
  path = require('path');

nconf.env().argv();

(nconf.get('dev') === true) ?
  nconf.file(path.join(__dirname, 'config.development.json'))
  : nconf.file(path.join(__dirname, 'config.production.json'));

module.exports = nconf;