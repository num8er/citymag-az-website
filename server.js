/* jshint strict: true */

"use strict";

require('newrelic');

// LOADING NECESSARY PACKAGES & COMPONENTS
const
  fs = require('fs'),
  http = require('http'),
  https = require('https'),
  config = require('./config'),
  app = require('./app'),
  cluster = require('./components/cluster');

// CERTIFICATE
const certificate = {
  key: fs.readFileSync('./certificates/' + config.get('app:https:certificate:key')),
  cert: fs.readFileSync('./certificates/' + config.get('app:https:certificate:cert'))
};

// STARTING APP IN CLUSTER WRAPPER
cluster.start(() => {
  // IF HTTP ENABLED LISTEN WITH HTTP MODULE
  if (config.get('app:http:enabled')) {
    let httpServer = http.createServer(app);
    let listenHost = config.get('app:http:host');
    let listenPort = config.get('app:http:port');
    httpServer.listen(listenPort, listenHost,
      () => console.log('App listening at http://%s:%s', listenHost, listenPort));
  }
  
  // IF HTTPS ENABLED LISTEN WITH HTTPS MODULE
  if (config.get('app:https:enabled')) {
    let httpsServer = https.createServer(certificate, app);
    let listenHost = config.get('app:https:host');
    let listenPort = config.get('app:https:port');
    httpsServer.listen(
      listenPort, listenHost,
      () => console.log('App listening at https://%s:%s', listenHost, listenPort));
  }
});