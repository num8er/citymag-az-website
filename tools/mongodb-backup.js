/* jshint strict: true */

"use strict";

const
  config = require('./../config'),
  exec = require('child_process').exec,
  fs = require('fs.extra'),
  path = require('path'),
  moment = require('moment');

let
  date = moment().format('YYYYMMDDHHmmss'),
  backupPath = path.join(__dirname, '..', 'backup', 'mongodb', date),
  cmd = 'mongodump';
  cmd+= ' --host='+config.get('db:host');
  cmd+= ' --port='+config.get('db:port');
  cmd+= ' --db='+config.get('db:name');
  cmd+= ' --username='+config.get('db:user');
  cmd+= ' --password='+config.get('db:pass');
  cmd+= ' --out='+backupPath;
  cmd+= ' --gzip'; // compress archive our collection output with Gzip

fs.mkdirpSync(backupPath);

console.log('MongoDB backup script');
console.log('>', cmd);
exec(cmd, (error) => {
  if (error) {
    console.error('exec error:', error);
    return;
  }

  console.log('Backup finished');
  console.log('Backup folder:', backupPath);
});