/* jshint strict: true */

"use strict";

const
  _ = require('lodash'),
  config = require('./../config'),
  shell = require('./../components/shell'),
  exec = require('child_process').exec,
  fs = require('fs.extra'),
  path = require('path'),
  moment = require('moment');

let
  backupFolder = path.join(__dirname, '..', 'backup', 'mongodb'),
  backups = fs.readdirSync(backupFolder)
                .filter((element) => {
                  let stat = fs.statSync(path.join(backupFolder, element));
                  return (stat && stat.isDirectory());
                });
  backups = _.takeRight(backups, 50);

console.log('MongoDB restore to remote server script');
console.log('CTRL+C to quit');

function pickBackupPoint() {
  shell.askOne({
    info: "\nPlease choose to which backup point You want to restore\n" +
    _.map(backups, (backup, n) => '[' + (n + 1) + '] ' + backup).join("\n") + "\n"
  }, (input) => {
    input = parseInt(_.trim(input));

    if (_.isNaN(input)) {
      console.error('Input not valid numeric number! Bye (:');
      return;
    }

    let backup = backups[input - 1];
    if(_.isEmpty(backup)) {
      console.error('Backup point with index: ['+input+'] not exists! Bye (:');
      return;
    }

    restoreFromBackup(backup);
  });
}

function restoreFromBackup(backup) {
  console.log("\nProceeding to restore from point: "+backup);
  shell.askOne({
    info: "Are You sure? (Y/N) "
  }, (input) => {
    input = _.toLower(_.trim(input));

    switch(input) {
      case 'y' :
        doRestore(backup);
        break;

      case 'n' :
        console.log('Restore cancelled by You. Bye (:');
        process.exit(0);
        break;

      default :
        console.error('Invalid answer! Asking You again.');
        restoreFromBackup(backup);
        break;
    }
  });
}

function doRestore(backup) {
  let
    restorePath = path.join(backupFolder, backup),
    cmd = 'mongorestore';
    cmd+= ' --host=127.0.0.1';
    cmd+= ' --port='+config.get('db:port');
    cmd+= ' --db='+config.get('db:name');
    cmd+= ' --username='+config.get('db:user');
    cmd+= ' --password='+config.get('db:pass');
    cmd+= ' --dir='+path.join(restorePath, config.get('db:name'));
    cmd+= ' --gzip'; // decompress gzipped input
    cmd+= ' --drop'; // drop each collection before import

  console.log('>', cmd);
  exec((cmd), (error) => {
    if(error) {
      console.error('exec error:', error);
      return;
    }

    console.log('Restore finished');
    console.log('Database state must be equal to data in folder:', restorePath);
    console.log("\n", 'Pleasure to be helpful. Bye (:');
  });
}

pickBackupPoint();