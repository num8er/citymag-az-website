# citymag-az-website
Website app for Citylife Magazine

------------

INSTALLATION

After pulling/cloning repo run following:

1) `npm install` to download required packages in `package.json` to `node_modules`

2) make sure You've configured copy of `config/config.json` as `config/config.local.json` (`config.local.json` will be added to gitignore soon)

3) make sure that `forever` package installed globally, otherwise run as root `npm i -g forever`

4) symlink or copy configured `vhost.conf` to nginx `/etc/nginx/sites-enable` (Debian like systems)

5) run app using: `./start.sh`

------------

TESTING

execution `./test.sh` will call tests in `spec` folder

!!! cover application with tests before pushing to `master` branch

------------