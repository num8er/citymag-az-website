/**
 * New Relic agent configuration.
 *
 * See lib/config.defaults.js in the agent distribution for a more complete
 * description of configuration variables and their potential values.
 */

const
  config = require('./config'),
  profilerConf = {
    /**
     * Array of application names.
     */
    app_name: [require('./package.json').name],
    /**
     * Your New Relic license key.
     */
    license_key: config.get('newrelic:license_key'),
    logging: {
      /**
       * Level at which to log. 'trace' is most useful to New Relic when diagnosing
       * issues with the agent, 'info' and higher will impose the least overhead on
       * production applications.
       */
      level: config.get('newrelic:level')
    }
  };


if(config.get('app:profiler') === true) {
  exports.config = profilerConf;
}